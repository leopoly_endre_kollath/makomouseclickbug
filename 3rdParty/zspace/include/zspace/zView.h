//////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2015 zSpace, Inc.  All Rights Reserved.
//
//  File:       zView.h
//  Content:    The zView public C API.
//  SVN Info:   $Id$
//
//////////////////////////////////////////////////////////////////////////

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// AUTO-GENERATED - DO NOT EDIT!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#ifndef __ZVIEW_H__
#define __ZVIEW_H__

#include "zSpaceTypes.h"


//////////////////////////////////////////////////////////////////////////
// Defines
//////////////////////////////////////////////////////////////////////////

#define ZVIEW_API_VERSION 1


//////////////////////////////////////////////////////////////////////////
// Handle Types
//////////////////////////////////////////////////////////////////////////

struct ZVContextOpaque;
/// @brief Opaque handle for zView API contexts.
///
/// @ingroup GeneralApis
typedef struct ZVContextOpaque* ZVContext;

struct ZVModeSpecOpaque;
/// @brief Opaque handle for zView mode specs.
///
/// @ingroup ModeApis
typedef struct ZVModeSpecOpaque* ZVModeSpec;

struct ZVModeOpaque;
/// @brief Opaque handle for zView modes.
///
/// @ingroup ModeApis
typedef const struct ZVModeOpaque* ZVMode;

struct ZVViewerConnectionOpaque;
/// @brief Opaque handle for zView connections.
///
/// @ingroup ConnectionManagementApis
typedef struct ZVViewerConnectionOpaque* ZVConnection;

struct ZVFrameOpaque;
/// @brief Opaque handle for zView frames.
///
/// @ingroup ConnectionFrameDataApis
typedef struct ZVFrameOpaque* ZVFrame;


//////////////////////////////////////////////////////////////////////////
// Enums
//////////////////////////////////////////////////////////////////////////

/// @brief Defines the error codes returned by all zView API functions.
///
/// @ingroup GeneralApis
typedef enum ZVError
{
    /// @brief No error occurred.
    ZV_ERROR_OK = 0,

    /// @brief An error of an unspecified type occurred.
    ZV_ERROR_FAILED = 1,

    /// @brief A zView API function that is not implemented was called.
    ///
    /// This may occur when running code written against a version of the zView
    /// API that is newer than the version of the zView runtime being used.
    ZV_ERROR_NOT_IMPLEMENTED = 2,

    /// @brief A zView API function was called before the zView runtime has
    /// been initialized.
    ZV_ERROR_NOT_INITIALIZED = 3,

    /// @brief A view API function was called with an invalid parameter value.
    ZV_ERROR_INVALID_PARAMETER = 4,

    /// @brief The zView runtime failed to allocate additional memory while
    /// performing some operation.
    ZV_ERROR_OUT_OF_MEMORY = 5,

    /// @brief A view API function was called with a buffer that is too small.
    ZV_ERROR_BUFFER_TOO_SMALL = 6,

    /// @brief The zview runtime DLL could not be found.
    ZV_ERROR_RUNTIME_NOT_FOUND = 7,

    /// @brief A required symbol within the zView runtime DLL could not be
    /// found.
    ZV_ERROR_SYMBOL_NOT_FOUND = 8,

    /// @brief A zView API function was called in a way that is not compatible
    /// with the version of the zView runtime being used.
    ZV_ERROR_RUNTIME_INCOMPATIBLE = 9,

    /// @brief A zView API function was called in a way that is not allowed for
    /// the current or specified node type.
    ZV_ERROR_INVALID_NODE_TYPE = 10,

    /// @brief A zView API function was called with an invalid ::ZVContext
    /// handle.
    ZV_ERROR_INVALID_CONTEXT = 11,

    /// @brief A zView API function was called in a way that is not allowed
    /// while the associated zView context is in its current state.
    ZV_ERROR_INVALID_CONTEXT_STATE = 12,

    /// @brief A zView API function was called with an invalid ::ZVModeSpec
    /// handle.
    ZV_ERROR_INVALID_MODE_SPEC = 13,

    /// @brief A zView API function was called with an invalid ::ZVMode handle.
    ZV_ERROR_INVALID_MODE = 14,

    /// @brief A zView API function was called with an invalid
    /// ::ZVModeAttributeKey enum value.
    ZV_ERROR_INVALID_MODE_ATTRIBUTE_KEY = 15,

    /// @brief An operation failed because an invalid connection specification
    /// was specified.
    ZV_ERROR_INVALID_CONNECTION_SPEC = 16,

    /// @brief A zView API function was called with an invalid ::ZVConnection
    /// handle.
    ZV_ERROR_INVALID_CONNECTION = 17,

    /// @brief A zView API function was called in a way that is not allowed
    /// while the associated zView connection is in its current state.
    ZV_ERROR_INVALID_CONNECTION_STATE = 18,

    /// @brief A zView API function was called with an invalid ::ZVSettingKey
    /// enum value.
    ZV_ERROR_INVALID_SETTING_KEY = 19,

    /// @brief A zView API function was called with an invalid ::ZVStream enum
    /// value.
    ZV_ERROR_INVALID_STREAM = 20,

    /// @brief A zView API function was called with an invalid ::ZVFrame
    /// handle.
    ZV_ERROR_INVALID_FRAME = 21,

    /// @brief A zView API function was called with an invalid ::ZVFrameDataKey
    /// enum value.
    ZV_ERROR_INVALID_FRAME_DATA_KEY = 22,

    /// @brief A zView API function was called in a way that is not allowed
    /// while the associated zView connection is in its current video recording
    /// state.
    ZV_ERROR_INVALID_VIDEO_RECORDING_STATE = 23,

    /// @brief An operation failed because the associated zView context has
    /// already been shut down.
    ZV_ERROR_SHUTDOWN = 24,

    /// @brief A zView API function was called with a ::ZVMode or ::ZVModeSpec
    /// handle representing a zView mode that is not supported.
    ZV_ERROR_UNSUPPORTED_MODE = 25,

    /// @brief A zView API function was called in a way that requires a
    /// capability that is not supported.
    ZV_ERROR_UNSUPPORTED_CAPABILITY = 26,

    /// @brief An operation failed because a low-level network I/O error
    /// occurred.
    ZV_ERROR_NETWORK = 27,

    /// @brief An operation failed because a zView communication protocol error
    /// occurred.
    ZV_ERROR_PROTOCOL = 28,

    /// @brief A zView connection could not be established because a
    /// communication protocol version supported by all nodes does not exist.
    ZV_ERROR_NO_SUPPORTED_PROTOCOL_VERSION = 29,
} ZVError;

/// @brief Defines the types of zView nodes that can exist.
///
/// @ingroup GeneralApis
typedef enum ZVNodeType
{
    ZV_NODE_TYPE_PRESENTER = 0,
    ZV_NODE_TYPE_VIEWER = 1,
} ZVNodeType;

/// @brief Defines the availability states for zView modes.
///
/// @ingroup GeneralApis
typedef enum ZVModeAvailability
{
    /// @brief Mode is available.
    ZV_MODE_AVAILABILITY_AVAILABLE = 0,

    /// @brief Mode is not available.
    ZV_MODE_AVAILABILITY_NOT_AVAILABLE = 1,

    /// @brief Mode is not available because no webcam hardware is available.
    ZV_MODE_AVAILABILITY_NOT_AVAILABLE_NO_WEBCAM = 2,

    /// @brief Mode is not available because necessary calibration has not been
    /// performed.
    ZV_MODE_AVAILABILITY_NOT_AVAILABLE_NOT_CALIBRATED = 3,
} ZVModeAvailability;

/// @brief Defines the optional capabilities that may be implemented by a zView
/// node.
///
/// @ingroup GeneralApis
typedef enum ZVCapability
{
    /// @brief The node supports video recording via the video recording APIs.
    ///
    /// A zView connection will support this capability as long as the viewer
    /// node in the connection supports this capability.  The presenter node
    /// need not support this capability.
    ZV_CAPABILITY_VIDEO_RECORDING = 0,

    /// @brief The node supports responding to requests to exit the application
    /// hosting the node when a zView connection is closed.
    ///
    /// For a zView node to support this capability, the application hosting
    /// the node must call zvGetConnectionCloseAction() whenever a zView
    /// connection enters the ::ZV_CONNECTION_STATE_CLOSED state and then exit
    /// if the close action is ::ZV_CONNECTION_CLOSE_ACTION_EXIT_APPLICATION.
    ZV_CAPABILITY_REMOTE_APPLICATION_EXIT = 1,
} ZVCapability;

/// @brief Defines the keys for all mode and mode spec attributes.
///
/// @ingroup ModeApis
typedef enum ZVModeAttributeKey
{
    /// @brief The version of the mode.  Datatype:  ::ZSUInt32.
    ///
    /// Different versions of a mode may function differently and may have
    /// different settings and frame data/buffers.
    ZV_MODE_ATTRIBUTE_KEY_VERSION = 0,

    /// @brief The mode's compositing mode.  Datatype:  ::ZVCompositingMode
    /// (get/set as ::ZSUInt32).
    ///
    /// This indicates how the viewer node should composite the images it
    /// receives from the presenter node.
    ZV_MODE_ATTRIBUTE_KEY_COMPOSITING_MODE = 1,

    /// @brief The mode's presenter camera mode.  Datatype:  ::ZVCameraMode
    /// (get/set as ::ZSUInt32).
    ///
    /// This indicates how the presenter node's camera should function when the
    /// mode is active.
    ZV_MODE_ATTRIBUTE_KEY_PRESENTER_CAMERA_MODE = 2,

    /// @brief The order of rows of pixels in images generated for the mode.
    /// Datatype:  ::ZVImageRowOrder (get/set as ::ZSUInt32).
    ///
    /// Any images generated by either the presenter or viewer node for the
    /// mode should have their rows of pixels in this order.
    ZV_MODE_ATTRIBUTE_KEY_IMAGE_ROW_ORDER = 3,

    /// @brief The format of pixels in color images generated for the mode.
    /// Datatype: ::ZVPixelFormat (get/set as ::ZSUInt32).
    ///
    /// Any color images generated by either the presenter or viewer node for
    /// the mode should use this pixel format.  The pixel format specifies the
    /// number, type, order, and size of the channels in a pixel.
    ZV_MODE_ATTRIBUTE_KEY_COLOR_IMAGE_PIXEL_FORMAT = 4,
} ZVModeAttributeKey;

/// @brief Defines the possible zView mode compositing modes.
///
/// This specifies the valid values that can be used to set the
/// ::ZV_MODE_ATTRIBUTE_KEY_COMPOSITING_MODE mode/mode spec attribute.
///
/// @ingroup ModeApis
typedef enum ZVCompositingMode
{
    /// @brief No compositing will be performed.
    ZV_COMPOSITING_MODE_NONE = 0,

    /// @brief Images will be composited on top of images from an augmented
    /// reality camera video stream.
    ZV_COMPOSITING_MODE_AUGMENTED_REALITY_CAMERA = 1,
} ZVCompositingMode;

/// @brief Defines the possible zView mode camera modes.
///
/// This currently specifies the valid values that can be used to set the
/// ::ZV_MODE_ATTRIBUTE_KEY_PRESENTER_CAMERA_MODE mode/mode spec attribute.
///
/// @ingroup ModeApis
typedef enum ZVCameraMode
{
    /// @brief The camera should have a fixed pose that never changes.
    ZV_CAMERA_MODE_FIXED = 0,

    /// @brief The camera's pose should change according to head tracking
    /// information obtained on the local zView node.
    ZV_CAMERA_MODE_LOCAL_HEAD_TRACKED = 1,

    /// @brief The camera's pose should change according to information sent
    /// from the remote zView node.
    ZV_CAMERA_MODE_REMOTE_MOVABLE = 2,
} ZVCameraMode;

/// @brief Defines the possible image pixel formats.
///
/// This currently specifies the valid values that can be used to set the
/// ::ZV_MODE_ATTRIBUTE_KEY_COLOR_IMAGE_PIXEL_FORMAT mode/mode spec attribute.
///
/// @ingroup ModeApis
typedef enum ZVPixelFormat
{
    /// @brief Pixel format with four 8-bit channels in the following order:
    /// red, green, blue, and alpha.
    ZV_PIXEL_FORMAT_R8_G8_B8_A8 = 0,
} ZVPixelFormat;

/// @brief Defines the possible orderings for pixel rows within images.
///
/// This currently specifies the valid values that can be used to set the
/// ::ZV_MODE_ATTRIBUTE_KEY_IMAGE_ROW_ORDER mode/mode spec attribute.
///
/// @ingroup ModeApis
typedef enum ZVImageRowOrder
{
    /// @brief The top row of pixels occurs first in the image data and the
    /// remaining rows are ordered from top to bottom.
    ZV_IMAGE_ROW_ORDER_TOP_TO_BOTTOM = 0,

    /// @brief The bottom row of pixels occurs first in the image data and the
    /// remaining rows are ordered from bottom to top.
    ZV_IMAGE_ROW_ORDER_BOTTOM_TO_TOP = 1,
} ZVImageRowOrder;

/// @brief Defines the possible zView connection states.
///
/// @ingroup ConnectionPropertyApis
typedef enum ZVConnectionState
{
    /// @brief The connection is initializing.
    ///
    /// In this state, client code should not perform any zView operations
    /// using the connection.
    ///
    /// The connection will automatically transition to the
    /// ::ZV_CONNECTION_STATE_AWAITING_CONNECTION_ACCEPTANCE state when it has
    /// finished initializing.
    ZV_CONNECTION_STATE_CONNECTION_INITIALIZATION = 0,

    /// @brief The connection is waiting to be locally accepted or rejected.
    ///
    /// In this state, client code should either accept the connection (by
    /// calling zvAcceptConnection()) or reject it (by calling
    /// zvCloseConnection()).
    ///
    /// If the connection is accepted, it will transition to the
    /// ::ZV_CONNECTION_STATE_SWITCHING_MODES state.  If the connection is
    /// rejected, it will transition to the ::ZV_CONNECTION_STATE_CLOSED state.
    ZV_CONNECTION_STATE_AWAITING_CONNECTION_ACCEPTANCE = 1,

    /// @brief The connection is internally switching between zView modes.
    ///
    /// In this state, client code is not required to perform any zView
    /// operations using the connection.
    ///
    /// The connection will automatically transition to the
    /// ::ZV_CONNECTION_STATE_NO_MODE state (if a switch to the NULL mode was
    /// requested) or to the ::ZV_CONNECTION_STATE_MODE_SETUP state (if a
    /// switch a non-NULL mode was requested) when it has finished switching
    /// modes internally.
    ZV_CONNECTION_STATE_SWITCHING_MODES = 2,

    /// @brief The connection is not currently in any zView mode.
    ///
    /// In this state, client code is not required to perform any zView
    /// operations using the connection.
    ///
    /// The connection can be switched into a mode by calling
    /// zvSetConnectionMode().
    ZV_CONNECTION_STATE_NO_MODE = 3,

    /// @brief The connection is setting up the current zView mode.
    ///
    /// In this state, client code should call zvGetConnectionModeSetupPhase()
    /// to determine which mode setup phase is currently active and perform the
    /// appropriate operations for that mode setup phase.
    ///
    /// The connection will transition to the ::ZV_CONNECTION_STATE_MODE_ACTIVE
    /// state once all mode setup phases for the current mode have been
    /// completed by both the local and remote nodes.
    ZV_CONNECTION_STATE_MODE_SETUP = 4,

    /// @brief The connection's current zView mode is active.
    ///
    /// In this state, client code should be sending frames to and/or receiving
    /// frames from the remote zView node.
    ///
    /// The connection's current zView mode can be paused by calling
    /// zvPauseMode().  This will transition the connection into the
    /// ::ZV_CONNECTION_STATE_MODE_PAUSED state.
    ZV_CONNECTION_STATE_MODE_ACTIVE = 5,

    /// @brief The connection's current zView mode is paused.
    ///
    /// In this state, client code should not be sending frames to nor
    /// receiving frames from the remote zView node.  Client code is not
    /// required to perform any zView operations while the connection is in
    /// this state.
    ///
    /// The connection's current zView mode can be resumed by calling
    /// zvResumeMode().  This will transition the connection into the
    /// ::ZV_CONNECTION_STATE_MODE_RESUMING state.
    ZV_CONNECTION_STATE_MODE_PAUSED = 6,

    /// @brief The connection's current zView mode is resuming.
    ///
    /// In this state, client code should not be sending frames to nor
    /// receiving frames from the remote zView node.  Client code is not
    /// required to perform any zView operations while the connection is in
    /// this state.
    ///
    /// The connection will automatically transition to the
    /// ::ZV_CONNECTION_STATE_MODE_ACTIVE state or to the
    /// ::ZV_CONNECTION_STATE_MODE_SETUP state when it has finished resuming
    /// the current mode.  The ::ZV_CONNECTION_STATE_MODE_SETUP state is only
    /// transitioned to if there changes have been made to mode-specific
    /// settings (while the mode was paused) that require one or more mode
    /// setup phases to be rerun.
    ZV_CONNECTION_STATE_MODE_RESUMING = 7,

    /// @brief The connection is internally processing a change to a
    /// mode-specific setting that will require one or more mode setup phases
    /// to be rerun.
    ///
    /// In this state, client code should not be sending frames to nor
    /// receiving frames from the remote zView node.  Client code is not
    /// required to perform any zView operations while the connection is in
    /// this state.
    ///
    /// The connection will automatically transition to the
    /// ::ZV_CONNECTION_STATE_MODE_SETUP state when it has finished internally
    /// processing the mode-specific settings change.
    ZV_CONNECTION_STATE_PROCESSING_MODE_SETTINGS_CHANGE = 8,

    /// @brief The connection is closed and should be cleaned up and destroyed.
    ///
    /// In this state, client code should clean up any state related to the
    /// connection and then destroy the connection by calling
    /// zvDestroyConnection().
    ///
    /// In this state, client code may call zvGetConnectionCloseReason() to
    /// determine why the connection was closed.
    ///
    /// In this state client code may call zvGetConnectionCloseAction() and
    /// then optionally perform the requested action.
    ZV_CONNECTION_STATE_CLOSED = 9,

    /// @brief An error has occurred and the connection is no longer usable.
    ///
    /// In this state, client code should clean up any state related to the
    /// connection and then destroy the connection by calling
    /// zvDestroyConnection().
    ///
    /// In this state, client code may call zvGetConnectionError() to
    /// determine the type of error that occurred.
    ZV_CONNECTION_STATE_ERROR = 10,
} ZVConnectionState;

/// @brief Defines the possible zView connection close actions.
///
/// When closing a connection by calling zvCloseConnection(), one of these
/// actions must be specified as the action that the remote zView node should
/// take after the connection is closed.
///
/// Once a connection enters the ::ZV_CONNECTION_STATE_CLOSED state, the close
/// action for the connection can be queried by calling
/// zvGetConnectionCloseAction().
///
/// @ingroup ConnectionPropertyApis
typedef enum ZVConnectionCloseAction
{
    /// @brief The application hosting the zView node should not perform any
    /// additional action after the connection is closed.
    ZV_CONNECTION_CLOSE_ACTION_NONE = 0,

    /// @brief The application hosting the zView node should exit after the
    /// connection is closed.
    ///
    /// zView nodes may not perform this action is they do not support the
    /// ::ZV_CAPABILITY_REMOTE_APPLICATION_EXIT capability.
    ZV_CONNECTION_CLOSE_ACTION_EXIT_APPLICATION = 1,
} ZVConnectionCloseAction;

/// @brief Defines the possible reasons why a zView connection was closed.
///
/// When closing a connection by calling zvCloseConnection(), one of these
/// reasons must be specified to indicate why the connection is being closed.
///
/// Once a connection enters the ::ZV_CONNECTION_STATE_CLOSED state, the close
/// reason for the connection can be queried by calling
/// zvGetConnectionCloseReason().
///
/// @ingroup ConnectionPropertyApis
typedef enum ZVConnectionCloseReason
{
    /// @brief The connection was closed for an unknown reason.
    ZV_CONNECTION_CLOSE_REASON_UNKNOWN = 0,

    /// @brief The connection was closed because the remote zView node's zView
    /// API context was shut down by calling zvShutDown().
    ZV_CONNECTION_CLOSE_REASON_SHUT_DOWN = 1,

    /// @brief The connection was closed because a user requested it to be
    /// closed.
    ZV_CONNECTION_CLOSE_REASON_USER_REQUESTED = 2,

    /// @brief The connection was closed because is was rejected by one of the
    /// zView nodes involved in the connection.
    ZV_CONNECTION_CLOSE_REASON_CONNECTION_REJECTED = 3,
} ZVConnectionCloseReason;

/// @brief Defines the possible phases that the setup of a zView mode can go
/// through.
///
/// The current mode setup phase of a zView connection can be queried by
/// calling zvGetConnectionModeSetupPhase() when the connection is in the
/// ::ZV_CONNECTION_STATE_MODE_SETUP state.
///
/// @note In this version of the zView API, all modes use all of the mode setup
/// phases defined by this enum.  However, future versions of the zView API may
/// introduce mode setup phases that are only used by some modes.
///
/// @ingroup ConnectionPhaseChangeApis
typedef enum ZVModeSetupPhase
{
    /// @brief Mode setup is initializing.
    ///
    /// When in this mode setup phase, client code must set any settings that
    /// the remote node will need to complete mode setup.  Exactly which
    /// settings must be set depends on the current mode.  Client code may also
    /// begin performing other setup tasks related to the current mode during
    /// this setup phase (e.g. client code might begin creating or loading
    /// resources needed for the current mode).
    ZV_MODE_SETUP_PHASE_INITIALIZATION = 0,

    /// @brief Mode setup is completing.
    ///
    /// When in this mode setup phase, client code must finish any setup that
    /// is necessary prior to the current mode becoming active.  Exactly what
    /// setup must be performed depends on the current mode.
    ZV_MODE_SETUP_PHASE_COMPLETION = 1,
} ZVModeSetupPhase;

/// @brief Defines the possible states that a connection setting can be in.
///
/// A setting's state can be queried by calling zvGetSettingState().
///
/// @ingroup ConnectionSettingsApis
typedef enum ZVSettingState
{
    /// @brief The setting's value is up to date.
    ZV_SETTING_STATE_UP_TO_DATE = 0,

    /// @brief The setting's value was changed during the most recent call to
    /// zvUpdateConnections().  State will transition to
    /// ::ZV_SETTING_STATE_UP_TO_DATE on the next call to zvUpdateConnections().
    ZV_SETTING_STATE_CHANGED = 1,

    /// @brief The setting's value has been changed locally, but the change has
    /// not yet been accepted by the other side of the connection.
    ZV_SETTING_STATE_CHANGE_PENDING = 2,
} ZVSettingState;

/// @brief Defines the keys for all possible zView connection settings.
///
/// @ingroup ConnectionSettingsApis
typedef enum ZVSettingKey
{
    /// @brief The width, in pixels, of the primary images for the connection's
    /// current mode.  Datatype:  ::ZSUInt16.
    ///
    /// In standard mode family modes, this should be set by the presenter node
    /// during the ::ZV_MODE_SETUP_PHASE_INITIALIZATION mode setup phase.  In
    /// augmented reality mode family modes, this should be set by the viewer
    /// node during the ::ZV_MODE_SETUP_PHASE_INITIALIZATION mode setup phase.
    ///
    /// If this setting is set after the ::ZV_MODE_SETUP_PHASE_INITIALIZATION
    /// mode setup phase (i.e. in a later mode setup phase or while the mode is
    /// active or paused), then the connection will automatically transition
    /// back to the ::ZV_CONNECTION_STATE_MODE_SETUP state in the
    /// ::ZV_MODE_SETUP_PHASE_COMPLETION mode setup phase in order to allow
    /// both nodes to take into account the new setting value (e.g. by
    /// reallocating image buffers to use the new width).
    ///
    /// This setting should generally be set in a batch (by calling
    /// zvBeginSettingsBatch() and zvEndSettingsBatch()) with the the
    /// ::ZV_SETTING_KEY_IMAGE_HEIGHT setting.  Doing this ensures that remote
    /// nodes will see image width and height changes at the same time (instead
    /// of possibly seeing one of these settings change during one frame and
    /// then the other change in the next frame).
    ZV_SETTING_KEY_IMAGE_WIDTH = 0,

    /// @brief The height, in pixels, of the primary images for the
    /// connection's current mode.  Datatype:  ::ZSUInt16.
    ///
    /// In standard mode family modes, this should be set by the presenter node
    /// during the ::ZV_MODE_SETUP_PHASE_INITIALIZATION mode setup phase.  In
    /// augmented reality mode family modes, this should be set by the viewer
    /// node during the ::ZV_MODE_SETUP_PHASE_INITIALIZATION mode setup phase.
    ///
    /// If this setting is set after the ::ZV_MODE_SETUP_PHASE_INITIALIZATION
    /// mode setup phase (i.e. in a later mode setup phase or while the mode is
    /// active or paused), then the connection will automatically transition
    /// back to the ::ZV_CONNECTION_STATE_MODE_SETUP state in the
    /// ::ZV_MODE_SETUP_PHASE_COMPLETION mode setup phase in order to allow
    /// both nodes to take into account the new setting value (e.g. by
    /// reallocating image buffers to use the new height).
    ///
    /// This setting should generally be set in a batch (by calling
    /// zvBeginSettingsBatch() and zvEndSettingsBatch()) with the the
    /// ::ZV_SETTING_KEY_IMAGE_WIDTH setting.  Doing this ensures that remote
    /// nodes will see image width and height changes at the same time (instead
    /// of possibly seeing one of these settings change during one frame and
    /// then the other change in the next frame).
    ZV_SETTING_KEY_IMAGE_HEIGHT = 1,

    /// @brief The connection's current video recording quality.  Datatype:
    /// ::ZVVideoRecordingQuality (get/set as ::ZSUInt32).
    ///
    /// This setting may only be set if the connection's video recording state
    /// is currently ::ZV_VIDEO_RECORDING_STATE_NOT_RECORDING.
    ///
    /// Whenever a video recording is started, the current value of this
    /// setting is used as the quality level for the new video recording.
    ZV_SETTING_KEY_VIDEO_RECORDING_QUALITY = 2,

    /// @brief The current horizontal offset, in pixels, of the primary image
    /// overlay displayed by the viewer node.  Datatype:  ::ZSFloat.
    ///
    /// This is only available for augmented reality mode family modes. For
    /// these modes the viewer node will use this value to offset the position
    /// of the presenter's color images from the augmented reality mode camera
    /// video stream images when the two are composited.
    ZV_SETTING_KEY_OVERLAY_OFFSET_X = 3,

    /// @brief The current vertical offset, in pixels, of the primary image
    /// overlay displayed by the viewer node.  Datatype:  ::ZSFloat.
    ///
    /// This is only available for augmented reality mode family modes. For
    /// these modes the viewer node will use this value to offset the position
    /// of the presenter's color images from the augmented reality mode camera
    /// video stream images when the two are composited.
    ZV_SETTING_KEY_OVERLAY_OFFSET_Y = 4,

    /// @brief The current horizontal scale factor for the primary image
    /// overlay displayed by the viewer node.  Datatype:  ::ZSFloat.
    ///
    /// This is only available for augmented reality mode family modes. For
    /// these modes the viewer node will use this value to scale the
    /// presenter's color images before they are composited with the augmented
    /// reality mode camera video stream images.
    ZV_SETTING_KEY_OVERLAY_SCALE_X = 5,

    /// @brief The current vertical scale factor for the primary image overlay
    /// displayed by the viewer node.  Datatype:  ::ZSFloat.
    ///
    /// This is only available for augmented reality mode family modes. For
    /// these modes the viewer node will use this value to scale the
    /// presenter's color images before they are composited with the augmented
    /// reality mode camera video stream images.
    ZV_SETTING_KEY_OVERLAY_SCALE_Y = 6,
} ZVSettingKey;

/// @brief Defines the possible streams that may be used by a zView mode for
/// sending frame data between the nodes involved in a zView connection.
///
/// @ingroup ConnectionFrameDataApis
typedef enum ZVStream
{
    /// @brief Stream used sending the primary image data between nodes for a
    /// mode.  May also be used to send metadata related to the images being
    /// sent or data necessary for generating images to be sent.
    ZV_STREAM_IMAGE = 0,
} ZVStream;

/// @brief Defines the keys for all possible pieces of frame data for a zView
/// mode.
///
/// @ingroup ConnectionFrameDataApis
typedef enum ZVFrameDataKey
{
    /// @brief The frame's frame number.  Datatype:  ::ZSUInt64.
    ///
    /// This is used for ::ZV_STREAM_IMAGE frames in both standard mode family
    /// and augmented reality mode family modes.
    ZV_FRAME_DATA_KEY_FRAME_NUMBER = 0,

    /// @brief The camera pose matrix to use for rendering the current mode's
    /// primary images.  Datatype:  ::ZSMatrix4.
    ///
    /// This is only used for ::ZV_STREAM_IMAGE frames sent from the viewer
    /// node to the presenter node in augmented reality mode family modes.
    ZV_FRAME_DATA_KEY_CAMERA_POSE = 1,

    /// @brief The camera focal length to use for rendering the current mode's
    /// primary images.  Datatype:  ::ZSFloat.
    ///
    /// This is only used for ::ZV_STREAM_IMAGE frames sent from the viewer
    /// node to the presenter node in augmented reality mode family modes.
    ZV_FRAME_DATA_KEY_CAMERA_FOCAL_LENGTH = 2,

    /// @brief The horizontal offset of the camera's principal point to use for
    /// rendering the current mode's primary images.  Datatype:  ::ZSFloat.
    ///
    /// This is only used for ::ZV_STREAM_IMAGE frames sent from the viewer
    /// node to the presenter node in augmented reality mode family modes.
    ZV_FRAME_DATA_KEY_CAMERA_PRINCIPAL_POINT_OFFSET_X = 3,

    /// @brief The vertical offset of the camera's principal point to use for
    /// rendering the current mode's primary images.  Datatype:  ::ZSFloat.
    ///
    /// This is only used for ::ZV_STREAM_IMAGE frames sent from the viewer
    /// node to the presenter node in augmented reality mode family modes.
    ZV_FRAME_DATA_KEY_CAMERA_PRINCIPAL_POINT_OFFSET_Y = 4,

    /// @brief The camera pixel aspect ratio to use for rendering the current
    /// mode's primary images.  Datatype:  ::ZSFloat.
    ///
    /// This is only used for ::ZV_STREAM_IMAGE frames sent from the viewer
    /// node to the presenter node in augmented reality mode family modes.
    ZV_FRAME_DATA_KEY_CAMERA_PIXEL_ASPECT_RATIO = 5,

    /// @brief The camera axis skew to use for rendering the current mode's
    /// primary images.  Datatype:  ::ZSFloat.
    ///
    /// This is only used for ::ZV_STREAM_IMAGE frames sent from the viewer
    /// node to the presenter node in augmented reality mode family modes.
    ZV_FRAME_DATA_KEY_CAMERA_AXIS_SKEW = 6,
} ZVFrameDataKey;

/// @brief Defines the keys for all possible frame buffers for a zView mode.
///
/// @ingroup ConnectionFrameDataApis
typedef enum ZVFrameBufferKey
{
    /// @brief Frame buffer for storing the first color image associated with a
    /// zView mode.
    ZV_FRAME_BUFFER_KEY_IMAGE_COLOR_0 = 0,
} ZVFrameBufferKey;

/// @brief Defines the possible zView connection video recording states.
///
/// A connection's current video recording state can be queried by calling
/// zvGetVideoRecordingState().
///
/// @ingroup VideoRecordingApis
typedef enum ZVVideoRecordingState
{
    /// @brief Video recording capability is not currently available and cannot
    /// be used.
    ///
    /// A connection's video recording state will automatically transition from
    /// this state to the ::ZV_VIDEO_RECORDING_STATE_NOT_RECORDING state if the
    /// connection supports the ::ZV_CAPABILITY_VIDEO_RECORDING capability.
    /// This transition will occur once the connection is fully initialized.
    ZV_VIDEO_RECORDING_STATE_NOT_AVAILABLE = 0,

    /// @brief Not actively recording and no current recording exists.
    ///
    /// A video recording can be started by calling zvStartVideoRecording().
    /// This will transition the video recording state to the
    /// ::ZV_VIDEO_RECORDING_STATE_STARTING state.
    ZV_VIDEO_RECORDING_STATE_NOT_RECORDING = 1,

    /// @brief Video recording is in the process of starting.
    ///
    /// A connection's video recording state will automatically transition from
    /// this state to the ::ZV_VIDEO_RECORDING_STATE_RECORDING state once video
    /// recording has fully started.
    ZV_VIDEO_RECORDING_STATE_STARTING = 2,

    /// @brief Actively recording.
    ///
    /// Recording can be finished by calling zvFinishVideoRecording(), which
    /// will transition the video recording state to the
    /// ::ZV_VIDEO_RECORDING_STATE_FINISHING state.  Recording can be paused by
    /// calling zvPauseVideoRecording(), which will transition the video
    /// recording state to the ::ZV_VIDEO_RECORDING_STATE_PAUSING state.
    ZV_VIDEO_RECORDING_STATE_RECORDING = 3,

    /// @brief Video recording is in the process of finishing.
    ///
    /// A connection's video recording state will automatically transition from
    /// this state to the ::ZV_VIDEO_RECORDING_STATE_FINISHED state once video
    /// recording has completed finishing.
    ZV_VIDEO_RECORDING_STATE_FINISHING = 4,

    /// @brief Not actively recording; current finished recording exists.
    ///
    /// The finished recording can be saved by calling zvSaveVideoRecording(),
    /// which will transition the video recording state to the
    /// ::ZV_VIDEO_RECORDING_STATE_SAVING state.  The finished recording can be
    /// discarded by calling zvDiscardVideoRecording(), which will transition
    /// the video recording state to the ::ZV_VIDEO_RECORDING_STATE_DISCARDING
    /// state.
    ZV_VIDEO_RECORDING_STATE_FINISHED = 5,

    /// @brief Video recording is in the process of pausing.
    ///
    /// A connection's video recording state will automatically transition from
    /// this state to the ::ZV_VIDEO_RECORDING_STATE_PAUSED state once video
    /// recording has completed pausing.
    ZV_VIDEO_RECORDING_STATE_PAUSING = 6,

    /// @brief Not actively recording; current resumable recording exists.
    ///
    /// Recording can be resumed by calling zvResumeVideoRecording(), which
    /// will transition the video recording state to the
    /// ::ZV_VIDEO_RECORDING_STATE_RESUMING state.  Recording can be finished by
    /// calling zvFinishVideoRecording(), which will transition the video
    /// recording state to the ::ZV_VIDEO_RECORDING_STATE_FINISHING state.
    ZV_VIDEO_RECORDING_STATE_PAUSED = 7,

    /// @brief Video recording is in the process of resuming.
    ///
    /// A connection's video recording state will automatically transition from
    /// this state to the ::ZV_VIDEO_RECORDING_STATE_RECORDING state once video
    /// recording has completed resuming.
    ZV_VIDEO_RECORDING_STATE_RESUMING = 8,

    /// @brief The current finished video recording is in the process of being
    /// saved.
    ///
    /// A connection's video recording state will automatically transition from
    /// this state to the ::ZV_VIDEO_RECORDING_STATE_NOT_RECORDING state once
    /// saving is complete.
    ZV_VIDEO_RECORDING_STATE_SAVING = 9,

    /// @brief The current finished video recording is in the process of being
    /// discarded.
    ///
    /// A connection's video recording state will automatically transition from
    /// this state to the ::ZV_VIDEO_RECORDING_STATE_NOT_RECORDING state once
    /// discarding is complete.
    ZV_VIDEO_RECORDING_STATE_DISCARDING = 10,

    /// @brief A recoverable video-recording-related error occurred.
    ///
    /// In this state, client code should call zvClearVideoRecordingError() to
    /// clear the error and allow new video recordings to be started.  This
    /// will transition the video recording state to the
    /// ::ZV_VIDEO_RECORDING_STATE_CLEARING_ERROR state.
    ///
    /// In this state, client code may call zvGetVideoRecordingError() to
    /// determine the type of error that occurred.
    ZV_VIDEO_RECORDING_STATE_ERROR = 11,

    /// @brief The most recent video recording error is in the process of being
    /// cleared.
    ///
    /// A connection's video recording state will automatically transition from
    /// this state to the ::ZV_VIDEO_RECORDING_STATE_NOT_RECORDING state once
    /// clearing of the video recording error is complete.
    ZV_VIDEO_RECORDING_STATE_CLEARING_ERROR = 12,
} ZVVideoRecordingState;

/// @brief Defines the possible video recording quality levels.
///
/// @ingroup VideoRecordingApis
typedef enum ZVVideoRecordingQuality
{
    /// @brief Video recording with 854 x 480 pixel resolution.
    ZV_VIDEO_RECORDING_QUALITY_480P = 0,

    /// @brief Video recording with 1280 x 720 pixel resolution.
    ZV_VIDEO_RECORDING_QUALITY_720P = 1,

    /// @brief Video recording with 1920 x 1080 pixel resolution.
    ZV_VIDEO_RECORDING_QUALITY_1080P = 2,
} ZVVideoRecordingQuality;


//////////////////////////////////////////////////////////////////////////
// Compound Types
//////////////////////////////////////////////////////////////////////////

/// @brief Struct representing a mode that is supported by a zView node along
/// with the current availability of that mode.
///
/// @ingroup GeneralApis
typedef struct ZVSupportedMode
{
    /// @brief The handle of the mode that is supported.
    ZVMode mode;
    /// @brief The supported mode's current availability.
    ZVModeAvailability availability;
} ZVSupportedMode;


//////////////////////////////////////////////////////////////////////////
// zView APIs
//////////////////////////////////////////////////////////////////////////

#ifndef ZVIEW_TYPES_ONLY

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////
/// @defgroup GeneralApis General APIs
///
/// @{
//////////////////////////////////////////////////////////////////////////

/// Initialize a zView context of the specified type.
///
/// @param[in] type The type of context to create.
/// @param[out] context The created context.
ZVError zvInitialize(ZVNodeType type, ZVContext* context);

/// Shut down the specified zView context.
///
/// @param[in] context The context to shut down.
ZVError zvShutDown(ZVContext context);

/// Get the version of the specified context.
///
/// @param[in] context The context to get the runtime version of.
/// @param[out] major The major version number component of the runtime
///                   version.
/// @param[out] minor The minor version number component of the runtime
///                   version.
/// @param[out] patch The patch version number component of the runtime
///                   version.
ZVError zvGetRuntimeVersion(ZVContext context, ZSInt32* major, ZSInt32* minor, ZSInt32* patch);

/// Get the size of the error string for the specified error code in bytes.
///
/// @param[in] error The error code to get the error string size for.
/// @param[out] size The size of the error string for the specified error code
///                  in bytes.
ZVError zvGetErrorStringSize(ZVError error, ZSInt32* size);

/// Get the error string for the specified error code.
///
/// @param[in] error The error code to get the error string for.
/// @param[inout] buffer The string buffer to fill with the error string.
/// @param[in] bufferSize The size of the specified buffer.
ZVError zvGetErrorString(ZVError error, char* buffer, ZSInt32 bufferSize);

/// Get the node type of the specified context.
///
/// @param[in] context The context to get the node type of.
/// @param[out] type The node type of the specified context.
ZVError zvGetNodeType(ZVContext context, ZVNodeType* type);

/// Get the size, in bytes, of the node ID for the specified context.
///
/// @param[in] context The context to get the node ID size of.
/// @param[out] size The size, in bytes, of the node ID for the specified
///                  context.
ZVError zvGetNodeIdSize(ZVContext context, ZSInt32* size);

/// Get the node ID of the specified context.
///
/// @param[in] context The context to get the node ID of.
/// @param[inout] buffer The byte buffer to fill with the node ID of the
///                      specified context.
/// @param[in] bufferSize The size of the specified buffer.
///
/// @note The specified buffer must contain at least the number of bytes
/// returned by the zvGetNodeIdSize() function.
ZVError zvGetNodeId(ZVContext context, ZSUInt8* buffer, ZSInt32 bufferSize);

/// Get the size, in bytes, of the UTF-8-encoded node name string associated
/// with the specified context.
///
/// @param[in] context The context to get the node name size of.
/// @param[out] size The size, in bytes, of the UTF-8-encoded node name string
///                  associated with the specified context.
///
/// @note The returned size will include space for a terminating null byte.
ZVError zvGetNodeNameSize(ZVContext context, ZSInt32* size);

/// Get the node name string associated with the specified context.
///
/// @param[in] context The context to get the node name of.
/// @param[inout] buffer The string buffer to fill with the node name
///                      associated with the specified context.
/// @param[in] bufferSize The size of the specified buffer.
///
/// The specified buffer will be filled with the UTF-8-encoded node name
/// associated with the specified context.  The node name string will be
/// terminated with a null byte and the specified buffer must have enough space
/// to store this null byte.
///
/// @note The specified buffer must contain at least the number of bytes
/// returned by the zvGetNodeNameSize() function.
ZVError zvGetNodeName(ZVContext context, char* buffer, ZSInt32 bufferSize);

/// Set the node name string associated with the specified context.
///
/// @param[in] context The context to set the node name of.
/// @param[in] name The new node name to use as a UTF-8-encoded, null-
///                 terminated string.
ZVError zvSetNodeName(ZVContext context, const char* name);

/// Get the size, in bytes, of the UTF-8-encoded node status string associated
/// with the specified context.
///
/// @param[in] context The context to get the node status size of.
/// @param[out] size The size, in bytes, of the UTF-8-encoded node status
///                  string associated with the specified context.
///
/// @note The returned size will include space for a terminating null byte.
ZVError zvGetNodeStatusSize(ZVContext context, ZSInt32* size);

/// Get the node status string associated with the specified context.
///
/// @param[in] context The context to get the node status of.
/// @param[inout] buffer The string buffer to fill with the node status
///                      associated with the specified context.
/// @param[in] bufferSize The size of the specified buffer.
///
/// The specified buffer will be filled with the UTF-8-encoded node status
/// associated with the specified context.  The node status string will be
/// terminated with a null byte and the specified buffer must have enough space
/// to store this null byte.
///
/// @note The specified buffer must contain at least the number of bytes
/// returned by the zvGetNodeNameSize() function.
ZVError zvGetNodeStatus(ZVContext context, char* buffer, ZSInt32 bufferSize);

/// Set the node status string associated with the specified context.
///
/// @param[in] context The context to set the node status of.
/// @param[in] status The new node status to use as a UTF-8-encoded, null-
///                   terminated string.
ZVError zvSetNodeStatus(ZVContext context, const char* status);

/// Get the number of modes supported by the specified context.
///
/// @param[in] context The context to get the number of supported modes for.
/// @param[out] numModes The number of modes in the specified context.
ZVError zvGetNumSupportedModes(ZVContext context, ZSInt32* numModes);

/// Get a supported mode from the list of supported modes for the specified
/// context.
///
/// @param[in] context The context to get the supported mode from.
/// @param[in] modeIndex The index of the supported mode to get.  This must be
///                      greater than or equal to 0 and less than the number of
///                      supported modes queried via zvGetNumSupportedModes().
/// @param[out] mode The requested supported mode.
ZVError zvGetSupportedMode(ZVContext context, ZSInt32 modeIndex, ZVSupportedMode* mode);

/// Set the modes supported by the specified context.
///
/// @param[in] context The context to set supported modes for.
/// @param[in] modes Pointer to an array of modes to set as supported along
///                  with mode availability information.
/// @param[in] numModes The number of modes in the specified supported modes
///                     array.
ZVError zvSetSupportedModes(ZVContext context, const ZVSupportedMode* modes, ZSInt32 numModes);

/// Get the number of capabilities supported by the specified context.
///
/// @param[in] context The context to get the number of supported capabilities
///                    for.
/// @param[out] numCapabilities The number of capabilities in the specified
///                             context.
ZVError zvGetNumSupportedCapabilities(ZVContext context, ZSInt32* numCapabilities);

/// Get a supported capability from the list of supported capabilities for the
/// specified context.
///
/// @param[in] context The context to get the supported capability from.
/// @param[in] capabilityIndex The index of the supported capability to get.
///                            This must be greater than or equal to 0 and less
///                            than the number of supported modes queried via
///                            zvGetNumSupportedCapabilities().
/// @param[out] capability The requested supported capability.
ZVError zvGetSupportedCapability(ZVContext context, ZSInt32 capabilityIndex, ZVCapability* capability);

/// Set the capabilities supported by the specified context.
///
/// @param[in] context The context to set supported capabilities for.
/// @param[in] capabilities Pointer to an array of capabilities to set as
///                         supported.
/// @param[in] numCapabilities The number of capabilities in the specified
///                            supported capabilities array.
ZVError zvSetSupportedCapabilities(ZVContext context, const ZVCapability* capabilities, ZSInt32 numCapabilities);

/// @}

//////////////////////////////////////////////////////////////////////////
/// @defgroup ModeApis Mode APIs
///
/// @{
//////////////////////////////////////////////////////////////////////////

/// Creates a new, default-initialized mode spec.
///
/// @param[in] context The context to use for creating the mode spec.
/// @param[in] modeSpec The created mode spec.
ZVError zvCreateModeSpec(ZVContext context, ZVModeSpec* modeSpec);

/// Destroys the specified mode spec.
///
/// @param[in] modeSpec The mode spec to destroy.
ZVError zvDestroyModeSpec(ZVModeSpec modeSpec);

/// Get the value of the specified mode attribute of type ZSUInt32 for the
/// specified mode spec.
///
/// @param[in] modeSpec The mode spec to get the attribute value of.
/// @param[in] key The mode attribute key to get the value of.
/// @param[out] value The value associated with specified mode spec and mode
///                   attribute key.
ZVError zvGetModeSpecAttributeU32(ZVModeSpec modeSpec, ZVModeAttributeKey key, ZSUInt32* value);

/// Set the value of the specified mode attribute of type ZSUInt32 for the
/// specified mode spec.
///
/// @param[in] modeSpec The mode spec to set the attribute value of.
/// @param[in] key The mode attribute key to set the value of.
/// @param[in] value The new value for the specified mode attribute key for the
///                  specified mode spec.
ZVError zvSetModeSpecAttributeU32(ZVModeSpec modeSpec, ZVModeAttributeKey key, ZSUInt32 value);

/// Gets the mode corresponds to the specified mode spec.
///
/// @param[in] modeSpec The mode spec to get the corresponding mode for.
/// @param[in] mode The mode corresponding to the specified mode spec.
ZVError zvGetModeForSpec(ZVModeSpec modeSpec, ZVMode* mode);

/// Get the value of the specified mode attribute of type ZSUInt32 for the
/// specified mode.
///
/// @param[in] mode The mode to get the attribute value of.
/// @param[in] key The mode attribute key to get the value of.
/// @param[out] value The value associated with specified mode and mode
///                   attribute key.
ZVError zvGetModeAttributeU32(ZVMode mode, ZVModeAttributeKey key, ZSUInt32* value);

/// @}

//////////////////////////////////////////////////////////////////////////
/// @defgroup ConnectionManagementApis Connection Management APIs
///
/// @{
//////////////////////////////////////////////////////////////////////////

/// Cause the specified context to start listening for connections.
///
/// @param[in] context The context that should start listening for connections.
/// @param[in] listeningSpec A listening specification string that indicates
///                          what communication mechanisms should be used to
///                          listen for connections.
///
/// If the specified context is a presenter context, then this cause it to
/// begin listening for viewer connections.  If the specified context is a
/// viewer context, then this will cause it to begin listening for presenter
/// connections.
///
/// This function can only be successfully called after the supported modes and
/// capabilities of the specified context have been set.
///
/// Calls to this function will fail if the specified context is already
/// listening for connections.
ZVError zvStartListeningForConnections(ZVContext context, const char* listeningSpec);

/// Cause the specified context to stop listening for connections.
///
/// @param[in] context The context that should stop listening for connections.
///
/// Calls to this function will fail if the specified context is not currently
/// listening for connections.
ZVError zvStopListeningForConnections(ZVContext context);

/// Connect to the default viewer using the specified context.
///
/// @param[in] context The context to connect to the default viewer.
/// @param[in] connectionUserData User data to be associated with the
///                               connection. This can be used to help identify
///                               connections when multiple connections are
///                               active.
///
/// This function performs its work asynchronously.  Once a connection to the
/// default viewer is created, it will be accessible via zvGetConnection()
/// after a call to zvUpdateConnectionList().
///
/// Viewer contexts are not supported. Additionally, if the default viewer is
/// not running, this function will attempt launch it.
ZVError zvConnectToDefaultViewer(ZVContext context, const void* connectionUserData);

/// Close the specified connection.
///
/// @param[in] connection The connection to close.
/// @param[in] action The action that should be performed by the remote node
///                   after the connection is closed.  This will be queriable
///                   by the remote node via the zvGetConnectionCloseAction()
///                   function once the connection has entered the
///                   ZV_CONNECTION_STATE_CLOSED state.  Note:  The node that
///                   calls this function for a connection will not be able to
///                   query the action it specifies via
///                   zvGetConnectionCloseAction(), since the action is meant
///                   for the remote node.  Instead,
///                   zvGetConnectionCloseAction() will always return
///                   ZV_CONNECTION_CLOSE_ACTION_NONE when called by the node
///                   that called this function.
/// @param[in] reason The reason why the connection is being closed. This will
///                   be queriable via the zvGetConnectionCloseReason()
///                   function once the connection has entered the
///                   ZV_CONNECTION_STATE_CLOSED state.
/// @param[in] reasonDetails Additional details on the reason why the
///                          connection is being closed.  This is purely for
///                          logging purposes and will not be displayed to the
///                          user.  This must be a UTF-8-encoded, null-
///                          terminated string.
///
/// This function performs its work asynchronously.  Once the specified
/// connection has been closed, it will enter the ZV_CONNECTION_STATE_CLOSED
/// state (which will only be visible after a call to zvUpdateConnection()) and
/// remain in this state until zvDestroyConnection() is called.
ZVError zvCloseConnection(ZVConnection connection, ZVConnectionCloseAction action, ZVConnectionCloseReason reason, const char* reasonDetails);

/// Accept the specified connection.
///
/// @param[in] connection The connection to accept.
///
/// This function is used to indicate that an application wishes to accept an
/// incoming connection from another application.
///
/// It is only valid to call this function on a connection in the
/// ZV_CONNECTION_STATE_AWAITING_CONNECTION_ACCEPTANCE state.
ZVError zvAcceptConnection(ZVConnection connection);

/// Destroy the specified connection.
///
/// @param[in] connection The connection to destroy.
///
/// This frees any resources associated with the connection and makes it so the
/// connection is no longer accessible via the zvGetConnection() function.
///
/// It is only valid to call this function on a connection in the
/// ZV_CONNECTION_STATE_CLOSED state.
ZVError zvDestroyConnection(ZVConnection connection);

/// Update the list of connections accessible via the zvGetNumConnections() and
/// zvGetConnection() functions to reflect the current set of active
/// connections for the node associated with the specified context at the time
/// this function is called.
///
/// @param[in] context The context to update the connection list for.
ZVError zvUpdateConnectionList(ZVContext context);

/// Get the number of currently visible connections for the specified context.
///
/// @param[in] context The context to get the number of connections for.
/// @param[out] numConnections The number of visible connections for the
///                            specified context.
///
/// The number of currently visible connections will change over the lifetime
/// of a context.  However, the number connections queried by this function
/// will only change when the zvUpdateConnectionList() function is called (i.e.
/// the value will remain stable between calls to zvUpdateConnectionList()).
ZVError zvGetNumConnections(ZVContext context, ZSInt32* numConnections);

/// Get a connection from the list of currently visible connections for the
/// specified context.
///
/// @param[in] context The context to get the connection from.
/// @param[in] connectionIndex The index of the connection to get.  This must
///                            be greater than or equal to 0 and less than the
///                            number of connections queried via
///                            zvGetNumConnections().
/// @param[out] connection The requested connection.
///
/// The list of currently visible connections will change over the lifetime of
/// a context.  However, the list of connections that is accessible via this
/// function will only change when the zvUpdateConnectionList() function is
/// called (i.e. the list will remain stable between calls to
/// zvUpdateConnectionList()).
ZVError zvGetConnection(ZVContext context, ZSInt32 connectionIndex, ZVConnection* connection);

/// Update the current connection state snapshot for the specified connection.
///
/// @param[in] connection The connection to update the connection state
///                       snapshot for.
ZVError zvUpdateConnection(ZVConnection connection);

/// @}

//////////////////////////////////////////////////////////////////////////
/// @defgroup ConnectionPropertyApis Connection Property APIs
///
/// @{
//////////////////////////////////////////////////////////////////////////

/// Get the state of the specified connection.
///
/// @param[in] connection The connection to get the state of.
/// @param[out] state The state of the specified connection.
///
/// The state of a connection will change over the lifetime of the connection.
/// However, the state queried via this function will only change when the
/// zvUpdateConnection() function is called (i.e. the state will remain stable
/// between calls to zvUpdateConnection()).
ZVError zvGetConnectionState(ZVConnection connection, ZVConnectionState* state);

/// Get the error code associated with the specified connection.  It is only
/// valid to query the error code associated with a connection when it is in
/// the ZV_CONNECTION_STATE_ERROR state.
///
/// @param[in] connection The connection to get the error code of.
/// @param[out] error The error code associated with the specified connection.
ZVError zvGetConnectionError(ZVConnection connection, ZVError* error);

/// Check whether the specified connection was initiated locally or remotely.
///
/// @param[in] connection The connection to check whether it was locally
///                       initiated.
/// @param[out] wasLocallyInitiated Whether the connection was locally
///                                 initiated.
ZVError zvWasConnectionLocallyInitiated(ZVConnection connection, ZSBool* wasLocallyInitiated);

/// Gets the size, in bytes, of the node ID of the remote node that the
/// specified connection is connected to.
///
/// @param[in] connection The connection to get the remote node ID size of.
/// @param[out] size The size, in bytes, of the node ID of the remote node that
///                  the specified connection is connected to.
ZVError zvGetConnectedNodeIdSize(ZVConnection connection, ZSInt32* size);

/// Gets the node ID of the remote node that the specified connection is
/// connected to.
///
/// @param[in] connection The connection to get the remote node ID of.
/// @param[inout] buffer The byte buffer to fill with the node ID of the remote
///                      node that the specified connection is connected to.
/// @param[in] bufferSize The size, in bytes, of the specified buffer.
ZVError zvGetConnectedNodeId(ZVConnection connection, ZSUInt8* buffer, ZSInt32 bufferSize);

/// Gets the size, in bytes, of the UTF-8-encoded node name string of the
/// remote node that the specified connection is connected to.
///
/// @param[in] connection The connection to get the remote node name size of.
/// @param[out] size The size, in bytes, of the UTF-8-encoded node name string
///                  of the remote node that the specified connection is
///                  connected to.
///
/// Note:  The returned size will include space for a terminating null byte.
ZVError zvGetConnectedNodeNameSize(ZVConnection connection, ZSInt32* size);

/// Gets the node name string of the remote node that the specified connection
/// is connected to.  The string will be UTF-8 encoded and null terminated.
///
/// @param[in] connection The connection to get the remote node name of.
/// @param[inout] buffer The buffer to fill with the node name of the remote
///                      node that the specified connection is connected to.
/// @param[in] bufferSize The size, in bytes, of the specified buffer.
ZVError zvGetConnectedNodeName(ZVConnection connection, char* buffer, ZSInt32 bufferSize);

/// Gets the size, in bytes, of the UTF-8-encoded node status string of the
/// remote node that the specified connection is connected to.
///
/// @param[in] connection The connection to get the remote node status size of.
/// @param[out] size The size, in bytes, of the UTF-8-encoded node status
///                  string of the remote node that the specified connection is
///                  connected to.
///
/// Note:  The returned size will include space for a terminating null byte.
ZVError zvGetConnectedNodeStatusSize(ZVConnection connection, ZSInt32* size);

/// Gets the node status string of the remote node that the specified
/// connection is connected to.  The string will be UTF-8 encoded and null
/// terminated.
///
/// @param[in] connection The connection to get the remote node status of.
/// @param[inout] buffer The buffer to fill with the node status of the remote
///                      node that the specified connection is connected to.
/// @param[in] bufferSize The size, in bytes, of the specified buffer.
ZVError zvGetConnectedNodeStatus(ZVConnection connection, char* buffer, ZSInt32 bufferSize);

/// Check if the specified connection supports the specified capability.
///
/// @param[in] connection The connection to check the capability support of.
/// @param[in] capability The capability to check for support of.
/// @param[out] isSupported Whether the specified capability is supported by
///                         the specified connection or not.
ZVError zvDoesConnectionSupportCapability(ZVConnection connection, ZVCapability capability, ZSBool* isSupported);

/// Get the number of modes supported by the specified connection.
///
/// @param[in] connection The connection to get the number of supported modes
///                       for.
/// @param[out] numSupportedModes The number of modes supported by the
///                               specified connection.
///
/// The number of modes supported by a connection may change over the lifetime
/// of the connection.  However, the number of supported modes returned by this
/// function will only change when the zvUpdateConnection() function is called
/// (i.e. the value will remain stable between calls to zvUpdateConnection()).
ZVError zvGetNumConnectionSupportedModes(ZVConnection connection, ZSInt32* numSupportedModes);

/// Get a supported mode and associated mode availability information from the
/// list of modes supported by the specified connection.
///
/// @param[in] connection The connection to get a supported mode from.
/// @param[in] supportedModeIndex The index of the supported mode to get.  This
///                               must be greater than or equal to 0 and less
///                               than the number of supported modes queried
///                               via the zvGetNumConnectionSupportedModes()
///                               function.
/// @param[out] supportedMode The requested supported mode with associated mode
///                           availability information.
///
/// The list of modes supported by a connection may change over the lifetime of
/// the connection.  However, the list of supported modes accessible via this
/// function will only change when the zvUpdateConnection() function is called
/// (i.e. the value will remain stable between calls to zvUpdateConnection()).
ZVError zvGetConnectionSupportedMode(ZVConnection connection, ZSInt32 supportedModeIndex, ZVSupportedMode* supportedMode);

/// Get the current mode of the specified connection.
///
/// @param[in] connection The connection to set the current mode of.
/// @param[out] mode The current mode of the specified connection.
///
/// The current mode of a connection will change over the lifetime of the
/// connection.  However, the mode queried via this function will only change
/// when the zvUpdateConnection() function is called (i.e. the value will
/// remain stable between calls to zvUpdateConnection()).
ZVError zvGetConnectionMode(ZVConnection connection, ZVMode* mode);

/// Set the current mode of the specified connection.
///
/// @param[in] connection The connection to set the current mode of.
/// @param[in] mode The new mode to use as the current mode of the specified
///                 connection.  Passing NULL for this argument makes it so
///                 that there is no current mode and transitions the
///                 connection into the ZV_CONNECTION_STATE_NO_MODE state.
///
/// If the current mode of the specified connection is not equal to the mode
/// specified when this function is called, then this function will initiate a
/// mode switch (the actual mode switch will occur asynchronously and will only
/// become visible after zvUpdateConnection() is called).
ZVError zvSetConnectionMode(ZVConnection connection, ZVMode mode);

/// Get the user data associated with the specified connection.
///
/// @param[in] connection The connection to get the user data of.
/// @param[out] userData The user data of the specified connection.
ZVError zvGetConnectionUserData(ZVConnection connection, const void** userData);

/// Set the user data associated with the specified connection.
///
/// @param[in] connection The connection to get the user data of.
/// @param[in] userData The user data of the specified connection.
ZVError zvSetConnectionUserData(ZVConnection connection, const void* userData);

/// Get the close action associated with the specified connection.
///
/// @param[in] connection The connection to get the close action of.
/// @param[out] action The close action of the specified connection.
///
/// The client code should perform this action if possible after a connection
/// enters the closed state.
///
/// It is only valid to call this function for a connection that is in the
/// ZV_CONNECTION_STATE_CLOSED state.
ZVError zvGetConnectionCloseAction(ZVConnection connection, ZVConnectionCloseAction* action);

/// Get the close reason associated with the specified connection.
///
/// @param[in] connection The connection to get the close reason of.
/// @param[out] reason The close reason of the specified connection.
///
/// It is only valid to call this function for a connection that is in the
/// ZV_CONNECTION_STATE_CLOSED state.
ZVError zvGetConnectionCloseReason(ZVConnection connection, ZVConnectionCloseReason* reason);

/// @}

//////////////////////////////////////////////////////////////////////////
/// @defgroup ConnectionPhaseChangeApis Connection Phase Change APIs
///
/// @{
//////////////////////////////////////////////////////////////////////////

/// Get the current mode setup phase for the specified connection as well as
/// whether the connection is waiting for something besides the client code to
/// complete the current mode setup phase.
///
/// @param[in] connection The connection to get the mode setup phase for.
/// @param[out] phase The mode setup phase for the specified connection.
/// @param[out] isAwaitingCompletion Whether the specified connection is
///                                  currently waiting for the remote node or
///                                  the zView runtime to complete the returned
///                                  mode setup phase.
///
/// It is only valid to call this function for a connection that is in the
/// ZV_CONNECTION_STATE_MODE_SETUP state.
ZVError zvGetConnectionModeSetupPhase(ZVConnection connection, ZVModeSetupPhase* phase, ZSBool* isAwaitingCompletion);

/// Signal that the specified mode setup phase is now complete for the
/// specified connection.
///
/// @param[in] connection The connection to complete the specified mode setup
///                       phase for.
/// @param[in] phase The mode setup phase to complete.
///
/// This allows the connection to advance to the next mode setup phase.  If
/// there are no mode setup phases remaining after the specified phase, calling
/// this function causes the connection to proceed with mode setup completion.
ZVError zvCompleteModeSetupPhase(ZVConnection connection, ZVModeSetupPhase phase);

/// Request that current mode be paused for the specified connection.
///
/// @param[in] connection The connection to pause frame sending for.
///
/// Pausing will occur asynchronously and eventually become visible after a
/// call to zvUpdateConnection().
///
/// It is only valid to call this function for a connection that is in the
/// ZV_CONNECTION_STATE_MODE_ACTIVE state.
ZVError zvPauseMode(ZVConnection connection);

/// Request that the current mode be resumed for the specified connection.
///
/// @param[in] connection The connection to resume frame sending for.
///
/// Resuming will occur asynchronously and eventually become visible after a
/// call to zvUpdateConnection().
///
/// It is only valid to call this function for a connection that is in the
/// ZV_CONNECTION_STATE_MODE_PAUSED state.
ZVError zvResumeMode(ZVConnection connection);

/// @}

//////////////////////////////////////////////////////////////////////////
/// @defgroup ConnectionSettingsApis Connection Settings APIs
///
/// @{
//////////////////////////////////////////////////////////////////////////

/// Begin a settings batch for the specified connection.
///
/// @param[in] connection The connection to begin a settings batch for.
///
/// While a settings batch is active for a connection, changes to setting
/// values will not be sent over the connection until the settings batch is
/// ended (via a call to zvEndSettingsBatch()).  This allows multiple settings
/// value changes to be sent as an atomic unit. This is necessary when a group
/// of settings are interrelated and changing one setting in the group requires
/// other settings in the group to also be changed in order to keep all
/// settings in the group in a consistent state.
///
/// At most one settings batch can be active at any time for a particular
/// connection.  Attempting to begin a settings batch for a connection when the
/// connection already has an active settings batch will result in an error.
ZVError zvBeginSettingsBatch(ZVConnection connection);

/// End a settings batch for the specified connection.
///
/// @param[in] connection The connection to end a settings batch for.
///
/// Attempting to end a setting batch for a connection that does not have an
/// active settings batch will result in an error.
ZVError zvEndSettingsBatch(ZVConnection connection);

/// Get the value of the specified setting of type ZSBool for the specified
/// connection.
///
/// @param[in] connection The connection to get the setting value for.
/// @param[in] key The setting key to get the value of.
/// @param[out] value The value associated with specified connection and
///                   setting key.
ZVError zvGetSettingB(ZVConnection connection, ZVSettingKey key, ZSBool* value);

/// Get the value of the specified setting of type ZSInt8 for the specified
/// connection.
///
/// @param[in] connection The connection to get the setting value for.
/// @param[in] key The setting key to get the value of.
/// @param[out] value The value associated with specified connection and
///                   setting key.
ZVError zvGetSettingI8(ZVConnection connection, ZVSettingKey key, ZSInt8* value);

/// Get the value of the specified setting of type ZSInt16 for the specified
/// connection.
///
/// @param[in] connection The connection to get the setting value for.
/// @param[in] key The setting key to get the value of.
/// @param[out] value The value associated with specified connection and
///                   setting key.
ZVError zvGetSettingI16(ZVConnection connection, ZVSettingKey key, ZSInt16* value);

/// Get the value of the specified setting of type ZSInt32 for the specified
/// connection.
///
/// @param[in] connection The connection to get the setting value for.
/// @param[in] key The setting key to get the value of.
/// @param[out] value The value associated with specified connection and
///                   setting key.
ZVError zvGetSettingI32(ZVConnection connection, ZVSettingKey key, ZSInt32* value);

/// Get the value of the specified setting of type ZSInt64 for the specified
/// connection.
///
/// @param[in] connection The connection to get the setting value for.
/// @param[in] key The setting key to get the value of.
/// @param[out] value The value associated with specified connection and
///                   setting key.
ZVError zvGetSettingI64(ZVConnection connection, ZVSettingKey key, ZSInt64* value);

/// Get the value of the specified setting of type ZSUInt8 for the specified
/// connection.
///
/// @param[in] connection The connection to get the setting value for.
/// @param[in] key The setting key to get the value of.
/// @param[out] value The value associated with specified connection and
///                   setting key.
ZVError zvGetSettingU8(ZVConnection connection, ZVSettingKey key, ZSUInt8* value);

/// Get the value of the specified setting of type ZSUInt16 for the specified
/// connection.
///
/// @param[in] connection The connection to get the setting value for.
/// @param[in] key The setting key to get the value of.
/// @param[out] value The value associated with specified connection and
///                   setting key.
ZVError zvGetSettingU16(ZVConnection connection, ZVSettingKey key, ZSUInt16* value);

/// Get the value of the specified setting of type ZSUInt32 for the specified
/// connection.
///
/// @param[in] connection The connection to get the setting value for.
/// @param[in] key The setting key to get the value of.
/// @param[out] value The value associated with specified connection and
///                   setting key.
ZVError zvGetSettingU32(ZVConnection connection, ZVSettingKey key, ZSUInt32* value);

/// Get the value of the specified setting of type ZSUInt64 for the specified
/// connection.
///
/// @param[in] connection The connection to get the setting value for.
/// @param[in] key The setting key to get the value of.
/// @param[out] value The value associated with specified connection and
///                   setting key.
ZVError zvGetSettingU64(ZVConnection connection, ZVSettingKey key, ZSUInt64* value);

/// Get the value of the specified setting of type ZSFloat for the specified
/// connection.
///
/// @param[in] connection The connection to get the setting value for.
/// @param[in] key The setting key to get the value of.
/// @param[out] value The value associated with specified connection and
///                   setting key.
ZVError zvGetSettingF32(ZVConnection connection, ZVSettingKey key, ZSFloat* value);

/// Get the value of the specified setting of type ZSDouble for the specified
/// connection.
///
/// @param[in] connection The connection to get the setting value for.
/// @param[in] key The setting key to get the value of.
/// @param[out] value The value associated with specified connection and
///                   setting key.
ZVError zvGetSettingF64(ZVConnection connection, ZVSettingKey key, ZSDouble* value);

/// Get the value of the specified setting of type ZSVector3 for the specified
/// connection.
///
/// @param[in] connection The connection to get the setting value for.
/// @param[in] key The setting key to get the value of.
/// @param[out] value The value associated with specified connection and
///                   setting key.
ZVError zvGetSettingV3(ZVConnection connection, ZVSettingKey key, ZSVector3* value);

/// Get the value of the specified setting of type ZSMatrix4 for the specified
/// connection.
///
/// @param[in] connection The connection to get the setting value for.
/// @param[in] key The setting key to get the value of.
/// @param[out] value The value associated with specified connection and
///                   setting key.
ZVError zvGetSettingM4(ZVConnection connection, ZVSettingKey key, ZSMatrix4* value);

/// Set the value of the specified setting of type ZSBool for the specified
/// connection.
///
/// @param[in] connection The connection to set the setting value for.
/// @param[in] key The setting key to set the value of.
/// @param[in] value The new value for the specified setting key for the
///                  specified connection.
///
/// When the value of a setting is set, its state will transition to
/// ZV_SETTING_STATE_CHANGE_PENDING until the new value has been accepted by
/// the other side of the associated connection.
ZVError zvSetSettingB(ZVConnection connection, ZVSettingKey key, ZSBool value);

/// Set the value of the specified setting of type ZSInt8 for the specified
/// connection.
///
/// @param[in] connection The connection to set the setting value for.
/// @param[in] key The setting key to set the value of.
/// @param[in] value The new value for the specified setting key for the
///                  specified connection.
///
/// When the value of a setting is set, its state will transition to
/// ZV_SETTING_STATE_CHANGE_PENDING until the new value has been accepted by
/// the other side of the associated connection.
ZVError zvSetSettingI8(ZVConnection connection, ZVSettingKey key, ZSInt8 value);

/// Set the value of the specified setting of type ZSInt16 for the specified
/// connection.
///
/// @param[in] connection The connection to set the setting value for.
/// @param[in] key The setting key to set the value of.
/// @param[in] value The new value for the specified setting key for the
///                  specified connection.
///
/// When the value of a setting is set, its state will transition to
/// ZV_SETTING_STATE_CHANGE_PENDING until the new value has been accepted by
/// the other side of the associated connection.
ZVError zvSetSettingI16(ZVConnection connection, ZVSettingKey key, ZSInt16 value);

/// Set the value of the specified setting of type ZSInt32 for the specified
/// connection.
///
/// @param[in] connection The connection to set the setting value for.
/// @param[in] key The setting key to set the value of.
/// @param[in] value The new value for the specified setting key for the
///                  specified connection.
///
/// When the value of a setting is set, its state will transition to
/// ZV_SETTING_STATE_CHANGE_PENDING until the new value has been accepted by
/// the other side of the associated connection.
ZVError zvSetSettingI32(ZVConnection connection, ZVSettingKey key, ZSInt32 value);

/// Set the value of the specified setting of type ZSInt64 for the specified
/// connection.
///
/// @param[in] connection The connection to set the setting value for.
/// @param[in] key The setting key to set the value of.
/// @param[in] value The new value for the specified setting key for the
///                  specified connection.
///
/// When the value of a setting is set, its state will transition to
/// ZV_SETTING_STATE_CHANGE_PENDING until the new value has been accepted by
/// the other side of the associated connection.
ZVError zvSetSettingI64(ZVConnection connection, ZVSettingKey key, ZSInt64 value);

/// Set the value of the specified setting of type ZSUInt8 for the specified
/// connection.
///
/// @param[in] connection The connection to set the setting value for.
/// @param[in] key The setting key to set the value of.
/// @param[in] value The new value for the specified setting key for the
///                  specified connection.
///
/// When the value of a setting is set, its state will transition to
/// ZV_SETTING_STATE_CHANGE_PENDING until the new value has been accepted by
/// the other side of the associated connection.
ZVError zvSetSettingU8(ZVConnection connection, ZVSettingKey key, ZSUInt8 value);

/// Set the value of the specified setting of type ZSUInt16 for the specified
/// connection.
///
/// @param[in] connection The connection to set the setting value for.
/// @param[in] key The setting key to set the value of.
/// @param[in] value The new value for the specified setting key for the
///                  specified connection.
///
/// When the value of a setting is set, its state will transition to
/// ZV_SETTING_STATE_CHANGE_PENDING until the new value has been accepted by
/// the other side of the associated connection.
ZVError zvSetSettingU16(ZVConnection connection, ZVSettingKey key, ZSUInt16 value);

/// Set the value of the specified setting of type ZSUInt32 for the specified
/// connection.
///
/// @param[in] connection The connection to set the setting value for.
/// @param[in] key The setting key to set the value of.
/// @param[in] value The new value for the specified setting key for the
///                  specified connection.
///
/// When the value of a setting is set, its state will transition to
/// ZV_SETTING_STATE_CHANGE_PENDING until the new value has been accepted by
/// the other side of the associated connection.
ZVError zvSetSettingU32(ZVConnection connection, ZVSettingKey key, ZSUInt32 value);

/// Set the value of the specified setting of type ZSUInt64 for the specified
/// connection.
///
/// @param[in] connection The connection to set the setting value for.
/// @param[in] key The setting key to set the value of.
/// @param[in] value The new value for the specified setting key for the
///                  specified connection.
///
/// When the value of a setting is set, its state will transition to
/// ZV_SETTING_STATE_CHANGE_PENDING until the new value has been accepted by
/// the other side of the associated connection.
ZVError zvSetSettingU64(ZVConnection connection, ZVSettingKey key, ZSUInt64 value);

/// Set the value of the specified setting of type ZSFloat for the specified
/// connection.
///
/// @param[in] connection The connection to set the setting value for.
/// @param[in] key The setting key to set the value of.
/// @param[in] value The new value for the specified setting key for the
///                  specified connection.
///
/// When the value of a setting is set, its state will transition to
/// ZV_SETTING_STATE_CHANGE_PENDING until the new value has been accepted by
/// the other side of the associated connection.
ZVError zvSetSettingF32(ZVConnection connection, ZVSettingKey key, ZSFloat value);

/// Set the value of the specified setting of type ZSDouble for the specified
/// connection.
///
/// @param[in] connection The connection to set the setting value for.
/// @param[in] key The setting key to set the value of.
/// @param[in] value The new value for the specified setting key for the
///                  specified connection.
///
/// When the value of a setting is set, its state will transition to
/// ZV_SETTING_STATE_CHANGE_PENDING until the new value has been accepted by
/// the other side of the associated connection.
ZVError zvSetSettingF64(ZVConnection connection, ZVSettingKey key, ZSDouble value);

/// Set the value of the specified setting of type ZSVector3 for the specified
/// connection.
///
/// @param[in] connection The connection to set the setting value for.
/// @param[in] key The setting key to set the value of.
/// @param[in] value The new value for the specified setting key for the
///                  specified connection.
///
/// When the value of a setting is set, its state will transition to
/// ZV_SETTING_STATE_CHANGE_PENDING until the new value has been accepted by
/// the other side of the associated connection.
ZVError zvSetSettingV3(ZVConnection connection, ZVSettingKey key, const ZSVector3* value);

/// Set the value of the specified setting of type ZSMatrix4 for the specified
/// connection.
///
/// @param[in] connection The connection to set the setting value for.
/// @param[in] key The setting key to set the value of.
/// @param[in] value The new value for the specified setting key for the
///                  specified connection.
///
/// When the value of a setting is set, its state will transition to
/// ZV_SETTING_STATE_CHANGE_PENDING until the new value has been accepted by
/// the other side of the associated connection.
ZVError zvSetSettingM4(ZVConnection connection, ZVSettingKey key, const ZSMatrix4* value);

/// Get the state of the specified setting for the specified connection.
///
/// @param[in] connection The connection to get the setting state for.
/// @param[in] key The setting key to get the state of.
/// @param[out] state The state of the specified setting state for the
///                   specified connection.
ZVError zvGetSettingState(ZVConnection connection, ZVSettingKey key, ZVSettingState* state);

/// @}

//////////////////////////////////////////////////////////////////////////
/// @defgroup ConnectionFrameDataApis Connection Frame Data APIs
///
/// @{
//////////////////////////////////////////////////////////////////////////

/// Attempt to receive a new frame from the specified stream over the specified
/// connection.
///
/// @param[in] connection The connection to receive a frame from.
/// @param[in] stream The stream to receive a frame from.
/// @param[out] frame The received frame, if there is one.  If no new frame is
///                   available, then this is set to NULL.
///
/// If this function is called and there is no new frame available for the
/// specified connection and stream, then a NULL frame is returned.
/// Additionally, if a previously received frame was not explicitly released
/// via zvReleaseReceivedFrame(), this function will generate an error and
/// return a NULL frame.
ZVError zvReceiveFrame(ZVConnection connection, ZVStream stream, ZVFrame* frame);

/// Release the specified received frame back to the connection and stream that
/// it is associated with.
///
/// @param[in] frame The frame to release.  This must be a frame that was
///                  returned by zvReceiveFrame().
ZVError zvReleaseReceivedFrame(ZVFrame frame);

/// Get the frame that should be used as the next frame to send for the
/// specified connection and stream.
///
/// @param[in] connection The connection to get the next frame to send for.
/// @param[in] stream The stream to get the next frame to send for.
/// @param[out] frame The next frame to send.  If this function is called
///                   multiple times for the same connection and stream before
///                   the returned frame is passed to zvSendFrame(), then an
///                   error will be generated and a NULL frame will be
///                   returned.
///
/// Client must set the necessary frame data and buffers on the frame returned
/// by this function before actually sending the frame.
ZVError zvGetNextFrameToSend(ZVConnection connection, ZVStream stream, ZVFrame* frame);

/// Send the specified frame over the connection and stream that it is
/// associated with.
///
/// @param[in] frame The frame to send.  This must be a frame that was returned
///                  by zvGetNextFrameToSend().
ZVError zvSendFrame(ZVFrame frame);

/// Get the value of the specified frame data of type ZSBool for the specified
/// frame.
///
/// @param[in] frame The frame to get frame data from.
/// @param[in] key The frame data key to get the value of.
/// @param[out] value The value associated with specified frame and frame data
///                   key.
ZVError zvGetFrameDataB(ZVFrame frame, ZVFrameDataKey key, ZSBool* value);

/// Get the value of the specified frame data of type ZSInt8 for the specified
/// frame.
///
/// @param[in] frame The frame to get frame data from.
/// @param[in] key The frame data key to get the value of.
/// @param[out] value The value associated with specified frame and frame data
///                   key.
ZVError zvGetFrameDataI8(ZVFrame frame, ZVFrameDataKey key, ZSInt8* value);

/// Get the value of the specified frame data of type ZSInt16 for the specified
/// frame.
///
/// @param[in] frame The frame to get frame data from.
/// @param[in] key The frame data key to get the value of.
/// @param[out] value The value associated with specified frame and frame data
///                   key.
ZVError zvGetFrameDataI16(ZVFrame frame, ZVFrameDataKey key, ZSInt16* value);

/// Get the value of the specified frame data of type ZSInt32 for the specified
/// frame.
///
/// @param[in] frame The frame to get frame data from.
/// @param[in] key The frame data key to get the value of.
/// @param[out] value The value associated with specified frame and frame data
///                   key.
ZVError zvGetFrameDataI32(ZVFrame frame, ZVFrameDataKey key, ZSInt32* value);

/// Get the value of the specified frame data of type ZSInt64 for the specified
/// frame.
///
/// @param[in] frame The frame to get frame data from.
/// @param[in] key The frame data key to get the value of.
/// @param[out] value The value associated with specified frame and frame data
///                   key.
ZVError zvGetFrameDataI64(ZVFrame frame, ZVFrameDataKey key, ZSInt64* value);

/// Get the value of the specified frame data of type ZSUInt8 for the specified
/// frame.
///
/// @param[in] frame The frame to get frame data from.
/// @param[in] key The frame data key to get the value of.
/// @param[out] value The value associated with specified frame and frame data
///                   key.
ZVError zvGetFrameDataU8(ZVFrame frame, ZVFrameDataKey key, ZSUInt8* value);

/// Get the value of the specified frame data of type ZSUInt16 for the
/// specified frame.
///
/// @param[in] frame The frame to get frame data from.
/// @param[in] key The frame data key to get the value of.
/// @param[out] value The value associated with specified frame and frame data
///                   key.
ZVError zvGetFrameDataU16(ZVFrame frame, ZVFrameDataKey key, ZSUInt16* value);

/// Get the value of the specified frame data of type ZSUInt32 for the
/// specified frame.
///
/// @param[in] frame The frame to get frame data from.
/// @param[in] key The frame data key to get the value of.
/// @param[out] value The value associated with specified frame and frame data
///                   key.
ZVError zvGetFrameDataU32(ZVFrame frame, ZVFrameDataKey key, ZSUInt32* value);

/// Get the value of the specified frame data of type ZSUInt64 for the
/// specified frame.
///
/// @param[in] frame The frame to get frame data from.
/// @param[in] key The frame data key to get the value of.
/// @param[out] value The value associated with specified frame and frame data
///                   key.
ZVError zvGetFrameDataU64(ZVFrame frame, ZVFrameDataKey key, ZSUInt64* value);

/// Get the value of the specified frame data of type ZSFloat for the specified
/// frame.
///
/// @param[in] frame The frame to get frame data from.
/// @param[in] key The frame data key to get the value of.
/// @param[out] value The value associated with specified frame and frame data
///                   key.
ZVError zvGetFrameDataF32(ZVFrame frame, ZVFrameDataKey key, ZSFloat* value);

/// Get the value of the specified frame data of type ZSDouble for the
/// specified frame.
///
/// @param[in] frame The frame to get frame data from.
/// @param[in] key The frame data key to get the value of.
/// @param[out] value The value associated with specified frame and frame data
///                   key.
ZVError zvGetFrameDataF64(ZVFrame frame, ZVFrameDataKey key, ZSDouble* value);

/// Get the value of the specified frame data of type ZSVector3 for the
/// specified frame.
///
/// @param[in] frame The frame to get frame data from.
/// @param[in] key The frame data key to get the value of.
/// @param[out] value The value associated with specified frame and frame data
///                   key.
ZVError zvGetFrameDataV3(ZVFrame frame, ZVFrameDataKey key, ZSVector3* value);

/// Get the value of the specified frame data of type ZSMatrix4 for the
/// specified frame.
///
/// @param[in] frame The frame to get frame data from.
/// @param[in] key The frame data key to get the value of.
/// @param[out] value The value associated with specified frame and frame data
///                   key.
ZVError zvGetFrameDataM4(ZVFrame frame, ZVFrameDataKey key, ZSMatrix4* value);

/// Set the value of the specified frame data of type ZSBool for the specified
/// frame.
///
/// @param[in] frame The frame to set frame data for.
/// @param[in] key The frame data key to set the value of.
/// @param[in] value The new value for the specified frame and frame data key.
ZVError zvSetFrameDataB(ZVFrame frame, ZVFrameDataKey key, ZSBool value);

/// Set the value of the specified frame data of type ZSInt8 for the specified
/// frame.
///
/// @param[in] frame The frame to set frame data for.
/// @param[in] key The frame data key to set the value of.
/// @param[in] value The new value for the specified frame and frame data key.
ZVError zvSetFrameDataI8(ZVFrame frame, ZVFrameDataKey key, ZSInt8 value);

/// Set the value of the specified frame data of type ZSInt16 for the specified
/// frame.
///
/// @param[in] frame The frame to set frame data for.
/// @param[in] key The frame data key to set the value of.
/// @param[in] value The new value for the specified frame and frame data key.
ZVError zvSetFrameDataI16(ZVFrame frame, ZVFrameDataKey key, ZSInt16 value);

/// Set the value of the specified frame data of type ZSInt32 for the specified
/// frame.
///
/// @param[in] frame The frame to set frame data for.
/// @param[in] key The frame data key to set the value of.
/// @param[in] value The new value for the specified frame and frame data key.
ZVError zvSetFrameDataI32(ZVFrame frame, ZVFrameDataKey key, ZSInt32 value);

/// Set the value of the specified frame data of type ZSInt64 for the specified
/// frame.
///
/// @param[in] frame The frame to set frame data for.
/// @param[in] key The frame data key to set the value of.
/// @param[in] value The new value for the specified frame and frame data key.
ZVError zvSetFrameDataI64(ZVFrame frame, ZVFrameDataKey key, ZSInt64 value);

/// Set the value of the specified frame data of type ZSUInt8 for the specified
/// frame.
///
/// @param[in] frame The frame to set frame data for.
/// @param[in] key The frame data key to set the value of.
/// @param[in] value The new value for the specified frame and frame data key.
ZVError zvSetFrameDataU8(ZVFrame frame, ZVFrameDataKey key, ZSUInt8 value);

/// Set the value of the specified frame data of type ZSUInt16 for the
/// specified frame.
///
/// @param[in] frame The frame to set frame data for.
/// @param[in] key The frame data key to set the value of.
/// @param[in] value The new value for the specified frame and frame data key.
ZVError zvSetFrameDataU16(ZVFrame frame, ZVFrameDataKey key, ZSUInt16 value);

/// Set the value of the specified frame data of type ZSUInt32 for the
/// specified frame.
///
/// @param[in] frame The frame to set frame data for.
/// @param[in] key The frame data key to set the value of.
/// @param[in] value The new value for the specified frame and frame data key.
ZVError zvSetFrameDataU32(ZVFrame frame, ZVFrameDataKey key, ZSUInt32 value);

/// Set the value of the specified frame data of type ZSUInt64 for the
/// specified frame.
///
/// @param[in] frame The frame to set frame data for.
/// @param[in] key The frame data key to set the value of.
/// @param[in] value The new value for the specified frame and frame data key.
ZVError zvSetFrameDataU64(ZVFrame frame, ZVFrameDataKey key, ZSUInt64 value);

/// Set the value of the specified frame data of type ZSFloat for the specified
/// frame.
///
/// @param[in] frame The frame to set frame data for.
/// @param[in] key The frame data key to set the value of.
/// @param[in] value The new value for the specified frame and frame data key.
ZVError zvSetFrameDataF32(ZVFrame frame, ZVFrameDataKey key, ZSFloat value);

/// Set the value of the specified frame data of type ZSDouble for the
/// specified frame.
///
/// @param[in] frame The frame to set frame data for.
/// @param[in] key The frame data key to set the value of.
/// @param[in] value The new value for the specified frame and frame data key.
ZVError zvSetFrameDataF64(ZVFrame frame, ZVFrameDataKey key, ZSDouble value);

/// Set the value of the specified frame data of type ZSVector3 for the
/// specified frame.
///
/// @param[in] frame The frame to set frame data for.
/// @param[in] key The frame data key to set the value of.
/// @param[in] value The new value for the specified frame and frame data key.
ZVError zvSetFrameDataV3(ZVFrame frame, ZVFrameDataKey key, const ZSVector3* value);

/// Set the value of the specified frame data of type ZSMatrix4 for the
/// specified frame.
///
/// @param[in] frame The frame to set frame data for.
/// @param[in] key The frame data key to set the value of.
/// @param[in] value The new value for the specified frame and frame data key.
ZVError zvSetFrameDataM4(ZVFrame frame, ZVFrameDataKey key, const ZSMatrix4* value);

/// Get a pointer to a data buffer for the specified frame and key.
///
/// @param[in] frame The frame to get the data buffer from.
/// @param[in] key The key to get the data buffer for.
/// @param[out] buffer The pointer to the data buffer for the specified key and
///                    frame.
///
/// If the specified frame was received by calling zvRecieveFrame(), then the
/// returned buffer pointer is intended to be read from.  If the specified
/// frame was acquired by calling zvGetNextFrameToSend(), then the returned
/// buffer pointer is intended to be written to.
ZVError zvGetFrameBuffer(ZVFrame frame, ZVFrameBufferKey key, ZSUInt8** buffer);

/// @}

//////////////////////////////////////////////////////////////////////////
/// @defgroup VideoRecordingApis Video Recording APIs
/// The APIs in this section are only available if the viewer associated with a
/// connection supports the ZV_CAPABILITY_VIDEO_RECORDING capability.
/// @{
//////////////////////////////////////////////////////////////////////////

/// Get the current video recording state of the specified connection.
///
/// @param[in] connection The connection to get the video recording state of.
/// @param[out] state The video recording state of the specified connection.
ZVError zvGetVideoRecordingState(ZVConnection connection, ZVVideoRecordingState* state);

/// Get the current video recording error code of the specified connection.
///
/// @param[in] connection The connection to get the video recording error code
///                       of.
/// @param[out] error The video recording error code of the specified
///                   connection.
///
/// Calling this function will fail unless the current video recording state is
/// ZV_VIDEO_RECORDING_STATE_ERROR.
ZVError zvGetVideoRecordingError(ZVConnection connection, ZVError* error);

/// Clear the video recording error code of the specified connection.
///
/// @param[in] connection The connection to clear the video recording error
///                       code of.
///
/// Transitions the video recording state of the connection from
/// ZV_VIDEO_RECORDING_STATE_ERROR to some other non-error state.  Exactly
/// which video recording state is transitioned to depends on the video
/// recording state that the connection was in prior to it entering the
/// ZV_VIDEO_RECORDING_STATE_ERROR state.  In most cases, this function will
/// transition the video recording state to
/// ZV_VIDEO_RECORDING_STATE_NOT_RECORDING.  A notable exception is when the
/// video recording state of the connection was ZV_VIDEO_RECORDING_STATE_SAVING
/// prior to it entering the ZV_VIDEO_RECORDING_STATE_ERROR state.  In this
/// case, the video recording state may transition back to
/// ZV_VIDEO_RECORDING_STATE_FINISHED after calling this function if a
/// recoverable save error occurred and it is still possible that the current
/// video recording could be saved (e.g. with a different file name or after
/// some disk space has been freed).
///
/// Calling this function will fail unless the current video recording state is
/// ZV_VIDEO_RECORDING_STATE_ERROR.
ZVError zvClearVideoRecordingError(ZVConnection connection);

/// Start video recording on the specified connection.
///
/// @param[in] connection The connection to start video recording on.
///
/// Transitions the video recording state of the connection to
/// ZV_VIDEO_RECORDING_STATE_RECORDING.  While the video recording state
/// transition is taking place, the video recording state will be
/// ZV_VIDEO_RECORDING_STATE_STARTING.
///
/// Calling this function will fail unless the current video recording state is
/// ZV_VIDEO_RECORDING_STATE_NOT_RECORDING.
ZVError zvStartVideoRecording(ZVConnection connection);

/// Finish video recording on the specified connection.
///
/// @param[in] connection The connection to finish video recording on.
///
/// Transitions the video recording state of the connection to
/// ZV_VIDEO_RECORDING_STATE_FINISHED.  While the video recording state
/// transition is taking place, the video recording state will be
/// ZV_VIDEO_RECORDING_STATE_FINISHING.
///
/// Calling this function will fail unless the current video recording state is
/// ZV_VIDEO_RECORDING_STATE_RECORDING or ZV_VIDEO_RECORDING_STATE_PAUSED.
ZVError zvFinishVideoRecording(ZVConnection connection);

/// Pause video recording on the specified connection.
///
/// @param[in] connection The connection to pause video recording on.
///
/// Transitions the video recording state of the connection to
/// ZV_VIDEO_RECORDING_STATE_PAUSED.  While the video recording state
/// transition is taking place, the video recording state will be
/// ZV_VIDEO_RECORDING_STATE_PAUSING.
///
/// Calling this function will fail unless the current video recording state is
/// ZV_VIDEO_RECORDING_STATE_RECORDING.
ZVError zvPauseVideoRecording(ZVConnection connection);

/// Resume video recording on the specified connection.
///
/// @param[in] connection The connection to resume video recording on.
///
/// Transitions the video recording state of the connection to
/// ZV_VIDEO_RECORDING_STATE_RECORDING.  While the video recording state
/// transition is taking place, the video recording state will be
/// ZV_VIDEO_RECORDING_STATE_RESUMING.
///
/// Calling this function will fail unless the current video recording state is
/// ZV_VIDEO_RECORDING_STATE_PAUSED.
ZVError zvResumeVideoRecording(ZVConnection connection);

/// Begin saving the specified connection's current video recording to the
/// specified file name.
///
/// @param[in] connection The connection to save the current video recording
///                       of.
/// @param[in] fileName The UTF-8-encoded, null-terminated file name to save
///                     the video recording to.
///
/// Transitions the video recording state of the connection to
/// ZV_VIDEO_RECORDING_STATE_SAVING.  Saving occurs asynchronously and the
/// video recording state of the connection will automatically transition to
/// ZV_VIDEO_RECORDING_STATE_NOT_RECORDING if saving finishes successfully.  If
/// saving fails, then the video recording state will automatically transition
/// to ZV_VIDEO_RECORDING_STATE_ERROR.
///
/// Calling this function will fail unless the current video recording state is
/// ZV_VIDEO_RECORDING_STATE_FINISHED.
ZVError zvSaveVideoRecording(ZVConnection connection, const char* fileName);

/// Begin discarding the specified connection's current video recording.
///
/// @param[in] connection The connection to discard the current video recording
///                       of.
///
/// Transitions the video recording state of the connection to
/// ZV_VIDEO_RECORDING_STATE_DISCARDING.  Discarding occurs asynchronously and
/// the video recording state of the connection will automatically transition
/// to ZV_VIDEO_RECORDING_STATE_NOT_RECORDING when discarding is finished.
///
/// Calling this function will fail unless the current video recording state is
/// ZV_VIDEO_RECORDING_STATE_FINISHED.
ZVError zvDiscardVideoRecording(ZVConnection connection);

/// Get the amount of time that has elapsed, in milliseconds, since the
/// specified connection's current video recording began.
///
/// @param[in] connection The connection to get the current video recording
///                       time of.
/// @param[out] timeInMilliseconds The current video recording time of the
///                                specified connection, in milliseconds.
///
/// Calling this function will fail unless the current video recording state is
/// one of the following: ZV_VIDEO_RECORDING_STATE_RECORDING,
/// ZV_VIDEO_RECORDING_STATE_FINISHING, ZV_VIDEO_RECORDING_STATE_FINISHED,
/// ZV_VIDEO_RECORDING_STATE_PAUSING, ZV_VIDEO_RECORDING_STATE_PAUSED,
/// ZV_VIDEO_RECORDING_STATE_RESUMING, ZV_VIDEO_RECORDING_STATE_SAVING,
/// ZV_VIDEO_RECORDING_STATE_DISCARDING.
ZVError zvGetVideoRecordingTime(ZVConnection connection, ZSUInt64* timeInMilliseconds);

/// @}

#ifdef __cplusplus
}
#endif

#endif // ZVIEW_TYPES_ONLY

#endif // __ZVIEW_H__

