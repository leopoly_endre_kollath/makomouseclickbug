//////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2007-2015 zSpace, Inc.  All Rights Reserved.
//
//  File:       zSpace.h
//  Content:    The zSpace SDK public C API.
//  SVN Info:   $Id$
//
//////////////////////////////////////////////////////////////////////////

#ifndef __ZSPACE_H__
#define __ZSPACE_H__

#include "zSpaceTypes.h"


//////////////////////////////////////////////////////////////////////////
// Defines
//////////////////////////////////////////////////////////////////////////

#define ZC_API_VERSION 2


#ifdef _WIN32
    #if (defined(ZSPACE_C_EXPORTS) && defined(ZSPACE_C_SHARED_LIB))
        #define ZC_C_API __declspec(dllexport)
    #elif defined(ZSPACE_C_SHARED_LIB)
        #define ZC_C_API __declspec(dllimport)
    #else
        #define ZC_C_API
    #endif
#else
    #define ZC_C_API
#endif


//////////////////////////////////////////////////////////////////////////
// Basic Types
//////////////////////////////////////////////////////////////////////////

typedef void* ZCContext;
typedef void* ZCHandle;


//////////////////////////////////////////////////////////////////////////
// Enums
//////////////////////////////////////////////////////////////////////////

/// @ingroup General
/// Defines the error codes returned by all Core SDK functions.
typedef enum ZCError
{
    ZC_ERROR_OK                   = 0,
    ZC_ERROR_NOT_IMPLEMENTED      = 1,
    ZC_ERROR_NOT_INITIALIZED      = 2,
    ZC_ERROR_ALREADY_INITIALIZED  = 3,
    ZC_ERROR_INVALID_PARAMETER    = 4,
    ZC_ERROR_INVALID_CONTEXT      = 5,
    ZC_ERROR_INVALID_HANDLE       = 6,
    ZC_ERROR_RUNTIME_INCOMPATIBLE = 7,
    ZC_ERROR_RUNTIME_NOT_FOUND    = 8,
    ZC_ERROR_SYMBOL_NOT_FOUND     = 9,
    ZC_ERROR_DISPLAY_NOT_FOUND    = 10,
    ZC_ERROR_DEVICE_NOT_FOUND     = 11,
    ZC_ERROR_TARGET_NOT_FOUND     = 12,
    ZC_ERROR_CAPABILITY_NOT_FOUND = 13,
    ZC_ERROR_BUFFER_TOO_SMALL     = 14,
    ZC_ERROR_SYNC_FAILED          = 15,
    ZC_ERROR_OPERATION_FAILED     = 16,
    ZC_ERROR_INVALID_ATTRIBUTE    = 17,
} ZCError;


/// @ingroup Display
/// Defines the types of displays for the Display APIs.
typedef enum ZCDisplayType
{
    ZC_DISPLAY_TYPE_UNKNOWN = -1,
    ZC_DISPLAY_TYPE_GENERIC =  0,
    ZC_DISPLAY_TYPE_ZSPACE  =  1
} ZCDisplayType;


/// @ingroup Display
/// Defines the attributes that you can query for the display.
/// See zcGetDisplayAttributeStr().
typedef enum ZCDisplayAttribute
{
    ZC_DISPLAY_ATTRIBUTE_ADAPTER_NAME       = 0,  ///< The graphics adapter name.
    ZC_DISPLAY_ATTRIBUTE_ADAPTER_STRING     = 1,  ///< The graphics adapter context string.
    ZC_DISPLAY_ATTRIBUTE_ADAPTER_ID         = 2,  ///< The entire ID string of the graphics adapter.
    ZC_DISPLAY_ATTRIBUTE_ADAPTER_VENDOR_ID  = 3,  ///< The vendor ID of the graphics adapter.
    ZC_DISPLAY_ATTRIBUTE_ADAPTER_DEVICE_ID  = 4,  ///< The device ID of the graphics adapter.
    ZC_DISPLAY_ATTRIBUTE_ADAPTER_KEY        = 5,  ///< Reserved.
    ZC_DISPLAY_ATTRIBUTE_MONITOR_NAME       = 6,  ///< The monitor name.
    ZC_DISPLAY_ATTRIBUTE_MONITOR_STRING     = 7,  ///< The monitor context string.
    ZC_DISPLAY_ATTRIBUTE_MONITOR_ID         = 8,  ///< The entire ID string of the monitor.
    ZC_DISPLAY_ATTRIBUTE_MONITOR_VENDOR_ID  = 9,  ///< The vendor ID of the monitor.
    ZC_DISPLAY_ATTRIBUTE_MONITOR_DEVICE_ID  = 10, ///< The device ID of the monitor.
    ZC_DISPLAY_ATTRIBUTE_MONITOR_KEY        = 11, ///< Reserved.
    ZC_DISPLAY_ATTRIBUTE_MANUFACTURER_NAME  = 12, ///< The display's manufacturer name.
    ZC_DISPLAY_ATTRIBUTE_PRODUCT_CODE       = 13, ///< The display's product code.
    ZC_DISPLAY_ATTRIBUTE_SERIAL_NUMBER      = 14, ///< The display's serial number.
    ZC_DISPLAY_ATTRIBUTE_VIDEO_INTERFACE    = 15, ///< The display's video interface.
    ZC_DISPLAY_ATTRIBUTE_MODEL              = 16, ///< The display's hardware model (currently only supported for zSpace displays).
} ZCDisplayAttribute;


/// @ingroup StereoBuffer
/// Defines the renderer used by the Stereo Buffer API.
typedef enum ZCRenderer
{
    ZC_RENDERER_QUAD_BUFFER_GL      = 0,
    ZC_RENDERER_QUAD_BUFFER_D3D9    = 1,
    ZC_RENDERER_QUAD_BUFFER_D3D10   = 2,
    ZC_RENDERER_QUAD_BUFFER_D3D11   = 3,
    ZC_RENDERER_QUAD_BUFFER_D3D11_1 = 4
} ZCRenderer;


/// @ingroup StereoFrustum 
/// Defines the eyes for the Stereo Frustum API.
/// This enum is also used by the Stereo Buffer API.
typedef enum ZCEye
{
    ZC_EYE_LEFT   =  0,
    ZC_EYE_RIGHT  =  1,
    ZC_EYE_CENTER =  2
} ZCEye;


/// @ingroup CoordinateSpace
/// Defines the coordinate spaces used by the zSpace Core SDK.
/// This enum is used by both the Coordinate Space API and 
/// the Stereo Frustum API.
typedef enum ZCCoordinateSpace
{
    ZC_COORDINATE_SPACE_TRACKER   =  0,
    ZC_COORDINATE_SPACE_DISPLAY   =  1,
    ZC_COORDINATE_SPACE_VIEWPORT  =  2,
    ZC_COORDINATE_SPACE_CAMERA    =  3
} ZCCoordinateSpace;


/// @ingroup StereoFrustum
/// Defines the attributes that you can set and query for the StereoFrustum.
/// These attributes are important for comfortable viewing of stereoscopic 3D.
typedef enum ZCFrustumAttribute
{
    ///////////////////////////////////////////
    // Float Attributes
    ///////////////////////////////////////////

    /// The physical separation, or inter-pupillary distance, between the eyes in meters.
    /// An IPD of 0 will effectively disable stereo since the eyes are assumed
    /// to be at the same location. (Default: 0.060)
    ZC_FRUSTUM_ATTRIBUTE_IPD                  = 0x00000000,

    /// Viewer scale adjusts the display and head tracking for larger and smaller scenes. (Default: 1)
    /// Use larger values for scenes with large models and smaller values for smaller models.
    ZC_FRUSTUM_ATTRIBUTE_VIEWER_SCALE         = 0x00000001,

    /// Uniform scale factor applied to the frustum's incoming head pose. (Default: 1)
    ZC_FRUSTUM_ATTRIBUTE_HEAD_SCALE           = 0x00000003,

    /// Near clipping plane for the frustum in meters. (Default: 0.1)
    ZC_FRUSTUM_ATTRIBUTE_NEAR_CLIP            = 0x00000004,

    /// Far clipping plane for the frustum in meters. (Default: 1000)
    ZC_FRUSTUM_ATTRIBUTE_FAR_CLIP             = 0x00000005,

    /// Distance between the bridge of the glasses and the bridge of the nose in meters. (Default: 0.01)
    ZC_FRUSTUM_ATTRIBUTE_GLASSES_OFFSET       = 0x00000006,

    /// Maximum pixel disparity for crossed images (negative parallax) in the coupled zone. (Default: -100)
    /// The coupled zone refers to the area where our eyes can both comfortably converge and focus on an object. 
    ZC_FRUSTUM_ATTRIBUTE_CC_LIMIT             = 0x00000007,

    /// Maximum pixel disparity for uncrossed images (positive parallax) in the coupled zone. (Default: 100)
    ZC_FRUSTUM_ATTRIBUTE_UC_LIMIT             = 0x00000008,

    /// Maximum pixel disparity for crossed images (negative parallax) in the uncoupled zone. (Default: -200)
    ZC_FRUSTUM_ATTRIBUTE_CU_LIMIT             = 0x00000009,

    /// Maximum pixel disparity for uncrossed images (positive parallax) in the uncoupled zone. (Default: 250)
    ZC_FRUSTUM_ATTRIBUTE_UU_LIMIT             = 0x0000000A,

    /// Maximum depth in meters for negative parallax in the coupled zone. (Default: 0.13)
    ZC_FRUSTUM_ATTRIBUTE_CC_DEPTH             = 0x0000000B,

    /// Maximum depth in meters for positive parallax in the coupled zone. (Default: -0.30)
    ZC_FRUSTUM_ATTRIBUTE_UC_DEPTH             = 0x0000000C,

    /// Display angle in degrees about the X axis. (Default: 30.0)
    /// Is only used when ZC_PORTAL_MODE_ANGLE is not enabled on the frustum.
    ZC_FRUSTUM_ATTRIBUTE_DISPLAY_ANGLE_X      = 0x0000000D,

    /// Display angle in degrees about the Y axis. (Default: 0.0)
    /// Is only used when ZC_PORTAL_MODE_ANGLE is not enabled on the frustum.
    ZC_FRUSTUM_ATTRIBUTE_DISPLAY_ANGLE_Y      = 0x0000000E,

    /// Display angle in degrees about the Z axis. (Default: 0.0)
    /// Is only used when ZC_PORTAL_MODE_ANGLE is not enabled on the frustum.
    ZC_FRUSTUM_ATTRIBUTE_DISPLAY_ANGLE_Z      = 0x0000000F,

    /// The delay in seconds before the automatic transition from stereo to mono begins. (Default: 5.0)
    ZC_FRUSTUM_ATTRIBUTE_AUTO_STEREO_DELAY    = 0x00000010,

    /// The duration in seconds of the automatic transition from stereo to mono. (Default: 1.0)
    ZC_FRUSTUM_ATTRIBUTE_AUTO_STEREO_DURATION = 0x00000011,


    ///////////////////////////////////////////
    // Boolean Attributes
    ///////////////////////////////////////////

    /// Flag controlling whether the automatic transition from stereo to mono is enabled. (Default: true)
    ZC_FRUSTUM_ATTRIBUTE_AUTO_STEREO_ENABLED  = 0x00010000,

} ZCFrustumAttribute;

/// @ingroup StereoFrustum
/// Defines options for positioning the scene relative to the physical display or relative to the viewport.
typedef enum ZCPortalMode
{
    ZC_PORTAL_MODE_NONE     = 0x00000000, ///< The scene is positioned relative to the viewport.
    ZC_PORTAL_MODE_ANGLE    = 0x00000001, ///< The scene's orientation is fixed relative to the physical desktop.
    ZC_PORTAL_MODE_POSITION = 0x00000002, ///< The scene's position is fixed relative to the center of the display.
    ZC_PORTAL_MODE_ALL      = 0xFFFFFFFF  ///< All portal modes except "none" are enabled.
} ZCPortalMode;


/// @ingroup TrackerTarget
/// Defines the types of tracker targets.
typedef enum ZCTargetType
{
    ZC_TARGET_TYPE_HEAD       =  0,  ///< The tracker target corresponding to the user's head.
    ZC_TARGET_TYPE_PRIMARY    =  1,  ///< The tracker target corresponding to the user's primary hand.
    ZC_TARGET_TYPE_SECONDARY  =  2   ///< The tracker target corresponding to the user's secondary hand. (Reserved for future use.)
} ZCTargetType;


/// @ingroup TrackerEvent
/// Defines event types that you can use with a tracker event handler.
typedef enum ZCTrackerEventType
{
    // Target Events
    ZC_TRACKER_EVENT_MOVE             = 0x0101,

    // Button Events
    ZC_TRACKER_EVENT_BUTTON_PRESS     = 0x0201,
    ZC_TRACKER_EVENT_BUTTON_RELEASE   = 0x0202,

    // Tap Events
    ZC_TRACKER_EVENT_TAP_PRESS        = 0x0301,
    ZC_TRACKER_EVENT_TAP_RELEASE      = 0x0302,
    ZC_TRACKER_EVENT_TAP_HOLD         = 0x0303,
    ZC_TRACKER_EVENT_TAP_SINGLE       = 0x0304,
    ZC_TRACKER_EVENT_TAP_DOUBLE       = 0x0305,

    // All Events
    ZC_TRACKER_EVENT_ALL              = 0xFFFF
} ZCTrackerEventType;


/// @ingroup MouseEmulation
/// Defines mouse buttons to be used when mapping a tracker target's buttons to a mouse.
typedef enum ZCMouseButton
{
    ZC_MOUSE_BUTTON_UNKNOWN = -1,
    ZC_MOUSE_BUTTON_LEFT    =  0,
    ZC_MOUSE_BUTTON_RIGHT   =  1,
    ZC_MOUSE_BUTTON_CENTER  =  2
} ZCMouseButton;


/// @ingroup MouseEmulation
/// Determines how the stylus and mouse control the cursor when both are used.
typedef enum ZCMouseMovementMode
{
    /// The stylus uses absolute positions.  
    /// In this mode, the mouse and stylus can fight for control of the cursor if both are in use.
    /// This is the default mode. 
    ZC_MOUSE_MOVEMENT_MODE_ABSOLUTE = 0,

    /// The stylus applies delta positions to the mouse cursor's current position.
    /// Movements by the mouse and stylus are compounded without fighting. 
    ZC_MOUSE_MOVEMENT_MODE_RELATIVE = 1
} ZCMouseMovementMode;


//////////////////////////////////////////////////////////////////////////
// Compound Types
//////////////////////////////////////////////////////////////////////////

// Ensure 8 byte packing.
#pragma pack( push, 8 )

/// @ingroup General
/// @brief Union representing an axis-aligned bounding box (AABB).
typedef union ZCBoundingBox
{
    ZSFloat f[6];
    struct 
    {
        ZSVector3 lower;  ///< The minimum extent, or back lower left, corner of the bounding box.
        ZSVector3 upper;  ///< The maximum extent, or front upper right, corner of the bounding box. 
    };
} ZCBoundingBox;


/// @ingroup Display
/// @brief Struct representing display intersection information.
typedef struct ZCDisplayIntersectionInfo
{
    ZSBool  hit;                ///< Whether or not the display was intersected.
    ZSInt32 x;                  ///< The x pixel coordinate on the virtual desktop.
    ZSInt32 y;                  ///< The y pixel coordinate on the virtual desktop.
    ZSInt32 nx;                 ///< The normalized absolute x pixel coordinate on the virtual desktop.
    ZSInt32 ny;                 ///< The normalized absolute y pixel coordinate on the virtual desktop.
    ZSFloat distance;           ///< The distance from origin of the raycast to the point of intersection on the display in meters.
} ZCDisplayIntersectionInfo;


/// @ingroup StereoFrustum
/// @brief Union representing frustum bounds.
typedef union ZCFrustumBounds
{
    ZSFloat f[6];
    struct
    {
        ZSFloat left;
        ZSFloat right;
        ZSFloat bottom;
        ZSFloat top;
        ZSFloat nearClip;
        ZSFloat farClip;
    };
} ZCFrustumBounds;


/// @ingroup TrackerTarget
/// @brief Struct representing tracker pose information.
///
/// This structure is used by the Tracker Target, Display, and Stereo Frustum APIs.
typedef struct ZCTrackerPose
{
    ZSDouble  timestamp;  ///< The time that the pose was captured (represented in seconds since last system reboot).
    ZSMatrix4 matrix;     ///< The tracker-space position and orientation in 4x4 matrix format.
} ZCTrackerPose;


/// @ingroup TrackerEvent
/// @brief The tracker event data type.
typedef struct ZCTrackerEventData
{
    ZSInt32            size;            ///< The event data's size in bytes.
    ZCTrackerEventType type;            ///< The event's type.
    ZSDouble           timestamp;       ///< The time that the event was generated (represented in seconds since last system reboot).
    ZSMatrix4          poseMatrix;      ///< The tracker-space position and orientation in 4x4 matrix format.
    ZSInt32            buttonId;        ///< The integer id corresponding to the target's physical button.
} ZCTrackerEventData;

#pragma pack( pop )


//////////////////////////////////////////////////////////////////////////
// Function Pointer Types
//////////////////////////////////////////////////////////////////////////

/// @ingroup TrackerEvent
/// Handler for tracker events.
/// 
/// @param[in] targetHandle  The handle to the tracker target that the event was generated for.
/// @param[in] eventData     A pointer to the event data.
/// @param[in] userData      A pointer to the user defined data that was passed in to the zcAddTrackerEventHandler() function. 
typedef void (*ZCTrackerEventHandler)(ZCHandle targetHandle, const ZCTrackerEventData* eventData, const void* userData);


//////////////////////////////////////////////////////////////////////////
// zSpace APIs
//////////////////////////////////////////////////////////////////////////

#ifndef ZSPACE_TYPES_ONLY

#ifdef __cplusplus
extern "C" {
#endif

// General API
ZC_C_API ZCError zcInitialize(ZCContext* context);
ZC_C_API ZCError zcUpdate(ZCContext context);
ZC_C_API ZCError zcShutDown(ZCContext context);
ZC_C_API ZCError zcGetContext(ZCHandle handle, ZCContext* context);
ZC_C_API ZCError zcSetTrackingEnabled(ZCContext context, ZSBool isEnabled);
ZC_C_API ZCError zcIsTrackingEnabled(ZCContext context, ZSBool* isEnabled);
ZC_C_API ZCError zcGetRuntimeVersion(ZCContext context, ZSInt32* major, ZSInt32* minor, ZSInt32* patch);
ZC_C_API ZCError zcGetErrorString(ZCError error, char* buffer, ZSInt32 bufferSize);

// Display API
ZC_C_API ZCError zcRefreshDisplays(ZCContext context);
ZC_C_API ZCError zcGetNumDisplays(ZCContext context, ZSInt32* numDisplays);
ZC_C_API ZCError zcGetNumDisplaysByType(ZCContext context, ZCDisplayType displayType, ZSInt32* numDisplays);
ZC_C_API ZCError zcGetDisplay(ZCContext context, ZSInt32 x, ZSInt32 y, ZCHandle* displayHandle);
ZC_C_API ZCError zcGetDisplayByIndex(ZCContext context, ZSInt32 index, ZCHandle* displayHandle);
ZC_C_API ZCError zcGetDisplayByType(ZCContext context, ZCDisplayType displayType, ZSInt32 index, ZCHandle* displayHandle);
ZC_C_API ZCError zcGetDisplayType(ZCHandle displayHandle, ZCDisplayType* displayType);
ZC_C_API ZCError zcGetDisplayNumber(ZCHandle displayHandle, ZSInt32* number);
ZC_C_API ZCError zcGetDisplayAdapterIndex(ZCHandle displayHandle, ZSInt32* adapterIndex);
ZC_C_API ZCError zcGetDisplayMonitorIndex(ZCHandle displayHandle, ZSInt32* monitorIndex);
ZC_C_API ZCError zcGetDisplayAttributeStr(ZCHandle displayHandle, ZCDisplayAttribute attribute, char* buffer, ZSInt32 bufferSize);
ZC_C_API ZCError zcGetDisplayAttributeStrSize(ZCHandle displayHandle, ZCDisplayAttribute attribute, ZSInt32* size);
ZC_C_API ZCError zcGetDisplaySize(ZCHandle displayHandle, ZSFloat* width, ZSFloat* height);
ZC_C_API ZCError zcGetDisplayPosition(ZCHandle displayHandle, ZSInt32* x, ZSInt32* y);
ZC_C_API ZCError zcGetDisplayNativeResolution(ZCHandle displayHandle, ZSInt32* x, ZSInt32* y);
ZC_C_API ZCError zcGetDisplayAngle(ZCHandle displayHandle, ZSFloat* x, ZSFloat* y, ZSFloat* z);
ZC_C_API ZCError zcGetDisplayVerticalRefreshRate(ZCHandle displayHandle, ZSFloat* refreshRate);
ZC_C_API ZCError zcIsDisplayHardwarePresent(ZCHandle displayHandle, ZSBool* isHardwarePresent);
ZC_C_API ZCError zcIntersectDisplay(ZCHandle displayHandle, const ZCTrackerPose* pose, ZCDisplayIntersectionInfo* intersectionInfo);

// StereoBuffer API
ZC_C_API ZCError zcCreateStereoBuffer(ZCContext context, ZCRenderer renderer, void* reserved, ZCHandle* bufferHandle);
ZC_C_API ZCError zcDestroyStereoBuffer(ZCHandle bufferHandle);
ZC_C_API ZCError zcSetStereoBufferFullScreen(ZCHandle bufferHandle, ZSBool isFullScreen);
ZC_C_API ZCError zcIsStereoBufferFullScreen(ZCHandle bufferHandle, ZSBool* isFullScreen);
ZC_C_API ZCError zcBeginStereoBufferFrame(ZCHandle bufferHandle);
ZC_C_API ZCError zcSyncStereoBuffer(ZCHandle bufferHandle);

// StereoViewport API
ZC_C_API ZCError zcCreateViewport(ZCContext context, ZCHandle* viewportHandle);
ZC_C_API ZCError zcDestroyViewport(ZCHandle viewportHandle);
ZC_C_API ZCError zcSetViewportPosition(ZCHandle viewportHandle, ZSInt32 x, ZSInt32 y);
ZC_C_API ZCError zcGetViewportPosition(ZCHandle viewportHandle, ZSInt32* x, ZSInt32* y);
ZC_C_API ZCError zcSetViewportSize(ZCHandle viewportHandle, ZSInt32 width, ZSInt32 height);
ZC_C_API ZCError zcGetViewportSize(ZCHandle viewportHandle, ZSInt32* width, ZSInt32* height);

// Coordinate Space API
ZC_C_API ZCError zcGetCoordinateSpaceTransform(ZCHandle viewportHandle, ZCCoordinateSpace a, ZCCoordinateSpace b, ZSMatrix4* transform);
ZC_C_API ZCError zcTransformMatrix(ZCHandle viewportHandle, ZCCoordinateSpace a, ZCCoordinateSpace b, ZSMatrix4* matrix);

// StereoFrustum API
ZC_C_API ZCError zcGetFrustum(ZCHandle viewportHandle, ZCHandle* frustumHandle);
ZC_C_API ZCError zcSetFrustumAttributeF32(ZCHandle frustumHandle, ZCFrustumAttribute attribute, ZSFloat value);
ZC_C_API ZCError zcGetFrustumAttributeF32(ZCHandle frustumHandle, ZCFrustumAttribute attribute, ZSFloat* value);
ZC_C_API ZCError zcSetFrustumAttributeB(ZCHandle frustumHandle, ZCFrustumAttribute attribute, ZSBool value);
ZC_C_API ZCError zcGetFrustumAttributeB(ZCHandle frustumHandle, ZCFrustumAttribute attribute, ZSBool* value);
ZC_C_API ZCError zcSetFrustumPortalMode(ZCHandle frustumHandle, ZSInt32 portalModeFlags);
ZC_C_API ZCError zcGetFrustumPortalMode(ZCHandle frustumHandle, ZSInt32* portalModeFlags);
ZC_C_API ZCError zcSetFrustumCameraOffset(ZCHandle frustumHandle, const ZSVector3* cameraOffset);
ZC_C_API ZCError zcGetFrustumCameraOffset(ZCHandle frustumHandle, ZSVector3* cameraOffset);
ZC_C_API ZCError zcSetFrustumHeadPose(ZCHandle frustumHandle, const ZCTrackerPose* headPose);
ZC_C_API ZCError zcGetFrustumHeadPose(ZCHandle frustumHandle, ZCTrackerPose* headPose);
ZC_C_API ZCError zcGetFrustumViewMatrix(ZCHandle frustumHandle, ZCEye eye, ZSMatrix4* viewMatrix);
ZC_C_API ZCError zcGetFrustumProjectionMatrix(ZCHandle frustumHandle, ZCEye eye, ZSMatrix4* projectionMatrix);
ZC_C_API ZCError zcGetFrustumBounds(ZCHandle frustumHandle, ZCEye eye, ZCFrustumBounds* bounds);
ZC_C_API ZCError zcGetFrustumEyePosition(ZCHandle frustumHandle, ZCEye eye, ZCCoordinateSpace coordinateSpace, ZSVector3* eyePosition);
ZC_C_API ZCError zcGetFrustumCoupledBoundingBox(ZCHandle frustumHandle, ZCBoundingBox* boundingBox);
ZC_C_API ZCError zcCalculateFrustumFit(ZCHandle frustumHandle, const ZCBoundingBox* boundingBox, ZSFloat* viewerScale, ZSMatrix4* lookAtMatrix);
ZC_C_API ZCError zcCalculateFrustumDisparity(ZCHandle frustumHandle, const ZSVector3* point, ZSFloat* disparity);

// TrackerDevice API
ZC_C_API ZCError zcGetNumTrackerDevices(ZCContext context, ZSInt32* numDevices);
ZC_C_API ZCError zcGetTrackerDevice(ZCContext context, ZSInt32 index, ZCHandle* deviceHandle);
ZC_C_API ZCError zcGetTrackerDeviceByName(ZCContext context, const char* deviceName, ZCHandle* deviceHandle);
ZC_C_API ZCError zcSetTrackerDeviceEnabled(ZCHandle deviceHandle, ZSBool isEnabled);
ZC_C_API ZCError zcIsTrackerDeviceEnabled(ZCHandle deviceHandle, ZSBool* isEnabled);
ZC_C_API ZCError zcGetTrackerDeviceName(ZCHandle deviceHandle, char* buffer, ZSInt32 bufferSize);
ZC_C_API ZCError zcGetTrackerDeviceNameSize(ZCHandle deviceHandle, ZSInt32* size);

// TrackerTarget API
ZC_C_API ZCError zcGetNumTargets(ZCHandle deviceHandle, ZSInt32* numTargets);
ZC_C_API ZCError zcGetNumTargetsByType(ZCContext context, ZCTargetType targetType, ZSInt32* numTargets);
ZC_C_API ZCError zcGetTarget(ZCHandle deviceHandle, ZSInt32 index, ZCHandle* targetHandle);
ZC_C_API ZCError zcGetTargetByName(ZCHandle deviceHandle, const char* targetName, ZCHandle* targetHandle);
ZC_C_API ZCError zcGetTargetByType(ZCContext context, ZCTargetType targetType, ZSInt32 index, ZCHandle* targetHandle);
ZC_C_API ZCError zcGetTargetName(ZCHandle targetHandle, char* buffer, ZSInt32 bufferSize);
ZC_C_API ZCError zcGetTargetNameSize(ZCHandle targetHandle, ZSInt32* size);
ZC_C_API ZCError zcSetTargetEnabled(ZCHandle targetHandle, ZSBool isEnabled);
ZC_C_API ZCError zcIsTargetEnabled(ZCHandle targetHandle, ZSBool* isEnabled);
ZC_C_API ZCError zcIsTargetVisible(ZCHandle targetHandle, ZSBool* isVisible);
ZC_C_API ZCError zcSetTargetMoveEventThresholds(ZCHandle targetHandle, ZSFloat time, ZSFloat distance, ZSFloat angle);
ZC_C_API ZCError zcGetTargetMoveEventThresholds(ZCHandle targetHandle, ZSFloat* time, ZSFloat* distance, ZSFloat* angle);
ZC_C_API ZCError zcGetTargetPose(ZCHandle targetHandle, ZCTrackerPose* pose);
ZC_C_API ZCError zcGetTargetTransformedPose(ZCHandle targetHandle, ZCHandle viewportHandle, ZCCoordinateSpace coordinateSpace, ZCTrackerPose* pose);
ZC_C_API ZCError zcSetTargetPoseBufferingEnabled(ZCHandle targetHandle, ZSBool isPoseBufferingEnabled);
ZC_C_API ZCError zcIsTargetPoseBufferingEnabled(ZCHandle targetHandle, ZSBool* isPoseBufferingEnabled);
ZC_C_API ZCError zcGetTargetPoseBuffer(ZCHandle targetHandle, ZSFloat minDelta, ZSFloat maxDelta, ZCTrackerPose* buffer, ZSInt32* bufferSize);
ZC_C_API ZCError zcResizeTargetPoseBuffer(ZCHandle targetHandle, ZSInt32 capacity);
ZC_C_API ZCError zcGetTargetPoseBufferCapacity(ZCHandle targetHandle, ZSInt32* capacity);

// TrackerTarget Button API
ZC_C_API ZCError zcGetNumTargetButtons(ZCHandle targetHandle, ZSInt32* numButtons);
ZC_C_API ZCError zcIsTargetButtonPressed(ZCHandle targetHandle, ZSInt32 buttonId, ZSBool* isButtonPressed);

// TrackerTarget Led API
ZC_C_API ZCError zcSetTargetLedEnabled(ZCHandle targetHandle, ZSBool isLedEnabled);
ZC_C_API ZCError zcIsTargetLedEnabled(ZCHandle targetHandle, ZSBool* isLedEnabled);
ZC_C_API ZCError zcIsTargetLedOn(ZCHandle targetHandle, ZSBool* isLedOn);
ZC_C_API ZCError zcSetTargetLedColor(ZCHandle targetHandle, ZSFloat r, ZSFloat g, ZSFloat b);
ZC_C_API ZCError zcGetTargetLedColor(ZCHandle targetHandle, ZSFloat* r, ZSFloat* g, ZSFloat* b);

// TrackerTarget Vibration API
ZC_C_API ZCError zcSetTargetVibrationEnabled(ZCHandle targetHandle, ZSBool isVibrationEnabled);
ZC_C_API ZCError zcIsTargetVibrationEnabled(ZCHandle targetHandle, ZSBool* isVibrationEnabled);
ZC_C_API ZCError zcIsTargetVibrating(ZCHandle targetHandle, ZSBool* isVibrating);
ZC_C_API ZCError zcStartTargetVibration(ZCHandle targetHandle, ZSFloat onPeriod, ZSFloat offPeriod, ZSInt32 numTimes, ZSFloat intensity);
ZC_C_API ZCError zcStopTargetVibration(ZCHandle targetHandle);

// TrackerTarget Tap API
ZC_C_API ZCError zcIsTargetTapPressed(ZCHandle targetHandle, ZSBool* isTapPressed);
ZC_C_API ZCError zcSetTargetTapHoldThreshold(ZCHandle targetHandle, ZSFloat seconds);
ZC_C_API ZCError zcGetTargetTapHoldThreshold(ZCHandle targetHandle, ZSFloat* seconds);

// Tracker Event API
ZC_C_API ZCError zcAddTrackerEventHandler(ZCHandle targetHandle, ZCTrackerEventType trackerEventType, ZCTrackerEventHandler trackerEventHandler, const void* userData);
ZC_C_API ZCError zcRemoveTrackerEventHandler(ZCHandle targetHandle, ZCTrackerEventType trackerEventType, ZCTrackerEventHandler trackerEventHandler, const void* userData);

// Mouse Emulation API
ZC_C_API ZCError zcSetMouseEmulationEnabled(ZCContext context, ZSBool isEnabled);
ZC_C_API ZCError zcIsMouseEmulationEnabled(ZCContext context, ZSBool* isEnabled);
ZC_C_API ZCError zcSetMouseEmulationTarget(ZCContext context, ZCHandle targetHandle);
ZC_C_API ZCError zcGetMouseEmulationTarget(ZCContext context, ZCHandle* targetHandle);
ZC_C_API ZCError zcSetMouseEmulationMovementMode(ZCContext context, ZCMouseMovementMode movementMode);
ZC_C_API ZCError zcGetMouseEmulationMovementMode(ZCContext context, ZCMouseMovementMode* movementMode);
ZC_C_API ZCError zcSetMouseEmulationMaxDistance(ZCContext context, ZSFloat maxDistance);
ZC_C_API ZCError zcGetMouseEmulationMaxDistance(ZCContext context, ZSFloat* maxDistance);
ZC_C_API ZCError zcSetMouseEmulationButtonMapping(ZCContext context, ZSInt32 buttonId, ZCMouseButton mouseButton);
ZC_C_API ZCError zcGetMouseEmulationButtonMapping(ZCContext context, ZSInt32 buttonId, ZCMouseButton* mouseButton);

#ifdef __cplusplus
}
#endif

#endif // ZSPACE_TYPES_ONLY

#endif // __ZSPACE_H__
