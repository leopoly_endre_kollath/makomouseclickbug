#include "ZSpaceManager.h"
#include <zspace/zSpace.h>
#include <iostream>


using namespace std;


// ------------------------------------------------------------------------
//  ZSpaceManager
// ------------------------------------------------------------------------
ZSpaceManager::ZSpaceManager() :
    m_initialized(false),
    m_context(nullptr),
    m_stylus(nullptr),
    m_updateThread(nullptr),
    m_terminateUpdateThread(false)
{
    initialize();
}

ZSpaceManager::~ZSpaceManager()
{
    shutDown();
}


static bool checkZCError(ZCError error)
{
    if (error != ZC_ERROR_OK)
    {
        char errorString[256];
        zcGetErrorString(error, errorString, sizeof(errorString));
        cerr << "ZSpace error: " << errorString << endl;
        return false;
    }
    return true;
}


#define CHECK_ERROR(error) \
    if (!checkZCError(error)) \
    { \
        shutDown(); \
    }


void ZSpaceManager::initialize()
{
    ZCError error = zcInitialize(&m_context);
    CHECK_ERROR(error);

    int major, minor, patch;
    error = zcGetRuntimeVersion(m_context, &major, &minor, &patch);
    CHECK_ERROR(error);

    error = zcGetTargetByType(m_context, ZC_TARGET_TYPE_PRIMARY, 0, &m_stylus);
    CHECK_ERROR(error);

    zcSetTargetVibrationEnabled(m_stylus, 1);

    m_initialized = true;
    printf("ZSpace initialized, runtime version: %d.%d.%d\n", major, minor, patch);
}

void ZSpaceManager::shutDown()
{
    enableMouseEmulation(false);
    zcSetTargetVibrationEnabled(m_stylus, false);
    zcShutDown(m_context);
}


bool ZSpaceManager::isInitialized() const
{
    return m_initialized;
}

void ZSpaceManager::update()
{
    if (!isUpdateThreadEnabled())
    {
        ZCError error = zcUpdate(m_context);
        if (error != ZC_ERROR_OK)
        {
            return;
        }
    }
}


void ZSpaceManager::startVibration(float onPeriod, float offPeriod, int numTimes, float intensity)
{
    zcStartTargetVibration(m_stylus, onPeriod, offPeriod, numTimes, intensity);
}

void ZSpaceManager::stopVibration()
{
    zcStopTargetVibration(m_stylus);
}


void ZSpaceManager::enableMouseEmulation(bool enable)
{
    zcSetMouseEmulationEnabled(m_context, enable);
    zcSetMouseEmulationTarget(m_context, m_stylus);
    zcSetMouseEmulationButtonMapping(m_context, (int)StylusButton::Left, ZC_MOUSE_BUTTON_LEFT);
    zcSetMouseEmulationButtonMapping(m_context, (int)StylusButton::Right, ZC_MOUSE_BUTTON_RIGHT);
    zcSetMouseEmulationButtonMapping(m_context, (int)StylusButton::Big, ZC_MOUSE_BUTTON_LEFT);
    enableUpdateThread(enable);
}

bool ZSpaceManager::isMouseEmulationEnabled() const
{
    ZSBool enabled;
    zcIsMouseEmulationEnabled(m_context, &enabled);
    return enabled > 0;
}


void ZSpaceManager::enableUpdateThread(bool enable)
{
    if (enable && !m_updateThread)
    {
        m_updateThread = new thread(&ZSpaceManager::updateThreadFunc, this);
    }
    else if (!enable)
    {
        if (m_updateThread)
        {
            m_terminateUpdateThread = true;
            m_updateThread->join();
            delete m_updateThread;
            m_updateThread = nullptr;
            m_terminateUpdateThread = false;
        }
    }
}

bool ZSpaceManager::isUpdateThreadEnabled()
{
    return m_updateThread != nullptr;
}

void ZSpaceManager::updateThreadFunc()
{
    while (!m_terminateUpdateThread)
    {
        zcUpdate(m_context);
        this_thread::sleep_for(std::chrono::milliseconds(16));
    }
}
// ------------------------------------------------------------------------