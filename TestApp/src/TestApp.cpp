#include <GLFW/glfw3.h>
#include <gl/GL.h>
#include "ZSpaceManager.h"


int main(int argc, const char** argv)
{
    GLFWwindow* window;

    if (!glfwInit())
    {
        return -1;
    }

    window = glfwCreateWindow(640, 480, "zSpace Test", nullptr, nullptr);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    ZSpaceManager* zsManager = new ZSpaceManager();
    if (zsManager->isInitialized())
    {
        zsManager->enableMouseEmulation(true);
    }

    while (!glfwWindowShouldClose(window))
    {
        if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT))
        {
            glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
        }
        else
        {
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        }
        glClear(GL_COLOR_BUFFER_BIT);
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    delete zsManager;

    glfwTerminate();

    return 0;
}
// ------------------------------------------------------------------------