#ifndef ZSPACE_MANAGER_H
#define ZSPACE_MANAGER_H

#include <functional>
#include <thread>


typedef void* ZCContext;
typedef void* ZCHandle;

namespace std
{
    class thread;
}

class ZSpaceManager
{
public:
    typedef std::function<void(bool visible)> VisibilityChangeFun;

    enum class StylusButton
    {
        Big = 0,
        Right = 1,
        Left = 2
    };

public:
    ZSpaceManager();
    ~ZSpaceManager();

    bool isInitialized() const;

    void update();

    void startVibration(float onPeriod, float offPeriod, int numTimes, float intensity = 1.0f);
    void stopVibration();

    void enableMouseEmulation(bool enable);
    bool isMouseEmulationEnabled() const;

    void enableUpdateThread(bool enable);
    bool isUpdateThreadEnabled();

private:
    void initialize();
    void shutDown();

    void updateThreadFunc();

private:
    bool m_initialized;

    ZCContext m_context;
    ZCHandle m_stylus;

    std::thread* m_updateThread;
    bool m_terminateUpdateThread;
};
// ------------------------------------------------------------------------

#endif