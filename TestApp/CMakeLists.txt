cmake_minimum_required (VERSION 3.0.0)

add_executable(TestApp src/TestApp.cpp src/ZSpaceManager.h src/ZSpaceManager.cpp)
target_include_directories(TestApp PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(TestApp PRIVATE ${CMAKE_SOURCE_DIR})
target_include_directories(TestApp PRIVATE ${3RDPARTY_FOLDER}/glfw/include)
target_include_directories(TestApp PRIVATE ${3RDPARTY_FOLDER}/zspace/include)

target_include_directories(TestApp PRIVATE ${3RDPARTY_FOLDER}/GLFW/include)
target_link_libraries(TestApp ${3RDPARTY_FOLDER}/glfw/lib-vc2017/glfw3.lib)
target_link_libraries(TestApp ${3RDPARTY_FOLDER}/zspace/lib/zSpaceApi.lib)
target_link_libraries(TestApp OpenGL32.lib)
